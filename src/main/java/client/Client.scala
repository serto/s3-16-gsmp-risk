package client

import java.awt.Toolkit
import java.util

import akka.actor.Actor
import javax.swing.{JFrame, JLabel, JPanel}
import message._
import risk.{Player, ResultGamePanel, RiskGUI, Territory}

/**
  * @author Matteo Sertori
  * @constructor create a new client
  * @param ipServer the IP of server
  *
  * Class that manage all type of Messsage. Thought this class user can interact with other user and server
  **/
class Client(ipServer: String) extends Actor {

  private val loadPanel = new JPanel
  private val frame = new JFrame
  private val waitString = new JLabel
  private val server = context.actorSelection("akka.tcp://ServerSystem@" + ipServer + ":5150/user/ActorServer")
  private val player = new Player
  private var listGlobal: util.ArrayList[Territory] = _
  private var riskGUI: RiskGUI = _

  var name = 0

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    server ! new GenericMessage("CONNECTION")
  }

  /**
    * Receive the message from the server or for the other player and manage the response
    */
  override def receive: Receive = {
    case msg: String =>
      msg match {
        case "CONNECTED2PLAYER" =>
          player.setOption(self, name)
        case "GETPLAYER" =>
          sender ! new MsgGetPlayerClient(this.player.getPlayerID)
        case "LOADING_COMPLETED" =>
          server ! new GenericMessage("LoadingMap")
        case "Press_next" =>
          server ! new MsgUpdateMap(riskGUI.getMapPanel.getTerritories)
        case "ATTACCO" =>
          server ! new GenericMessage("MsgAttack")
        case "FINE ATTACCO" =>
          server ! new GenericMessage("NoAttack")
        case "ATTACCO_COMPLETATO" =>
          server ! new MsgSendUpdateMapAfterAttack(riskGUI.getMapPanel.getTerritories)
        case "DEFENCE" =>
          server ! new GenericMessage("DefenceTurn")
        case "RIPROVA_ATTACCO" =>
          server ! new GenericMessage("NewAttack")
        case "FINE_TURNO" =>
          server ! new GenericMessage("EndTurn")
        case "WINNER" =>
          server ! new MsgResultGame("WINNER")
      }
        case msg: MsgNameClient =>
      name = msg.getMessage

    case msg: GenericMessage =>
      msg.getMessage match {
        case "CONNECTED" =>
          server ! new GenericMessage("GETNAMECLIENT")

          frame.setBounds(Toolkit.getDefaultToolkit.getScreenSize.width / 2, Toolkit.getDefaultToolkit.getScreenSize.height / 2, 300, 100)
          waitString.setText("WAITING FOR CONNECTION")

          loadPanel.add(waitString)
          frame.add(loadPanel)
          frame.setVisible(true)
        case "StartGame" =>
          riskGUI.getMapPanel.enabledButtons(true)
          loadPanel.setVisible(false)
        case "MsgAttack" =>
          riskGUI.getPlayerPanel.getPnlDefenceManager.setPnlStateDefence()
        case "DefenceTurn" =>
          riskGUI.getPlayerPanel.setStateGame(2) //DEFENCE
          if(loadPanel.isVisible)
            loadPanel.setVisible(false)
        case "EndTurn" =>
          riskGUI.getPlayerPanel.setStateGame(1) //ATTACK
          riskGUI.getMapPanel.getMapManager.state.setState_(1) //RENFORCE
          riskGUI.getMapPanel.enabledButtons(true)
          riskGUI.getPlayerPanel.getPnlAttackManager.getPnlReinforce.setPanelComponent_ON()
        case "NewAttack" =>
          riskGUI.getPlayerPanel.getPnlDefenceManager.setPnlAfterDices()
        case "NoAttack" =>
          riskGUI.getPlayerPanel.getPnlDefenceManager.setPnlStateShift()
      }

    case msg: DeleteTerritoryInList =>
      server ! new MsgDeleteTerritoryInList(msg.getIdTer)

    case msg: MsgDeleteTerritoryInList =>
      player.subtractTerritory(msg.getIdTer)

    case msg: MsgTargetCreated =>
      player.setTarget(msg.getTarget)

    case msg: TerritoriesPlayer =>
      player.setOption(self, name)
      player.populateTerritoriesList(msg.getPlayerTerritories)

      listGlobal = msg.getGlobalTerritories.asInstanceOf[util.ArrayList[Territory]]

      riskGUI = new RiskGUI(listGlobal, player)

    case msg: MsgUpdateMap =>
      riskGUI.getMapPanel.updateTerritories(msg.getMessage)

    case msg: MsgSendUpdateMapAfterAttack =>
      riskGUI.getMapPanel.updateTerritories(msg.getMessage)

    case msg: SendAttackCarried =>
      server ! new MsgSendAttackCarried(msg.getIdTerAttack, msg.getIdTerDefence, msg.getnArmiesToAttacking)

    case msg: MsgSendAttackCarried =>
      riskGUI.getPlayerPanel.getPnlDefenceManager.getPnlStateDefence.setPanelComponent_ON()
      riskGUI.getPlayerPanel.getPnlDefenceManager.getPnlStateDefence.setLblTerritories(msg.getIdTerAttack, msg.getIdTerDefence, msg.getnArmiesToAttacking)

    case msg: SendDiceResult =>
      server ! new MsgSendDiceResult(msg.getWinner, msg.getWinsD, msg.getWinsA)

    case msg: MsgSendDiceResult =>
      riskGUI.getPlayerPanel.getPnlDefenceManager.getPnlStateDefence.setPanelResult_ON()
      riskGUI.getPlayerPanel.getPnlDefenceManager.getPnlStateDefence.setResult(msg.getWinner, msg.getWinsD, msg.getWinsA)

    case msg: SendShiftArmies =>
      server ! new MsgSendShiftArmies(msg.getShiftFrom, msg.getShiftTo, msg.getTerritories)

    case msg: MsgSendShiftArmies =>
      riskGUI.getPlayerPanel.getPnlDefenceManager.getPnlStateShift.setLblTerritories(msg.getShiftFrom, msg.getShiftTo)
      riskGUI.getMapPanel.updateTerritories(msg.getTerritories)

    case msg: MsgResultGame =>
      new ResultGamePanel(msg.getMessage)

  }
}