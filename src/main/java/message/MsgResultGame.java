package message;

import scala.Serializable;

/**
 * Message who contain if the user win or lose
 *
 * @author Matteo Sertori
 */
public class MsgResultGame implements Serializable {

    private String result;

    /**
     * @param result The string that contains "WINNER" or "LOSER"
     */
    public MsgResultGame(String result){
        this.result = result;
    }

    /**
     * Return the string of result game
     *
     * @return the result string of user game
     */
    public String getMessage(){
        return this.result;
    }
}
