package message;

import scala.Serializable;

/**
 * Message that send the result of Dice(the winner user, how many armies lost the defense and how many armies lost
 * the attack).
 *
 * @author Matteo Sertori
 */
public class SendDiceResult implements Serializable {

    private String winner;
    private int winsD, winsA;

    /**
     * @param winner the winner user
     * @param winsD how many armies lost the defense
     * @param winsA how many armies lost the attack
     */
    public SendDiceResult(String winner, int winsD, int winsA){

        this.winner = winner;
        this.winsA = winsA;
        this.winsD = winsD;
    }

    /**
     * Get the winner user
     *
     * @return the winner user
     */
    public String getWinner(){return this.winner;}

    /**
     * Get how many armies lost the defense
     *
     * @return how many armies lost the defense
     */
    public int getWinsD(){ return this.winsD;}

    /**
     * Get how many armies lost the attack
     *
     * @return how many armies lost the attack
     */
    public int getWinsA(){ return this.winsA;}
}
