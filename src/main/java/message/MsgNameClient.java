package message;

import scala.Serializable;

/**
 * Message who contains the ID of player
 *
 * @author Matteo Sertori
 */
public class MsgNameClient implements Serializable {

    private int playerID;

    /**
     * @param playerID the PlayerID
     */
    public MsgNameClient(int playerID){
        this.playerID = playerID;
    }

    /**
     * Get the playerID
     *
     * @return the playerID
     */
    public int getMessage(){
        return this.playerID;
    }
}
