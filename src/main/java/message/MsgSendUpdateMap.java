package message;

import risk.Territory;
import scala.Serializable;

import java.util.ArrayList;

/**
 * Message who send the map updated after an attack or at the end of reinforce turn
 *
 * @author Matteo Sertori
 */
public class MsgSendUpdateMap implements Serializable {

    private ArrayList<Territory> territories;

    /**
     * @param territories list of all territories
     */
    public MsgSendUpdateMap(ArrayList<Territory> territories){
        this.territories = territories;
    }

    /**
     * Return the list of all territories
     *
     * @return get the list of all territories
     */
    public ArrayList<Territory> getMessage(){return this.territories;}
}
