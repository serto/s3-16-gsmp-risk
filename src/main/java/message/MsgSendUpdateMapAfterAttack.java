package message;

import risk.Territory;
import scala.Serializable;
import java.util.ArrayList;

/**
 * Message that send the Territory after player attack
 *
 * @author Matteo Sertori
 */
public class MsgSendUpdateMapAfterAttack implements Serializable {

    private ArrayList<Territory> territories;

    /**
     * @param territories the list of territories
     */
    public MsgSendUpdateMapAfterAttack(ArrayList<Territory> territories){
        this.territories = territories;
    }

    /**
     * @return the list of territories
     */
    public ArrayList<Territory> getMessage(){return this.territories;}
}

