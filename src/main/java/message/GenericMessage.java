package message;

import scala.Serializable;

/**
 *  Generic message of control
 */
public class GenericMessage implements Serializable {

    private String message;

    /**
     * @param message the message
     */
    public GenericMessage(String message){ this.message = message; }

    /**
     * Return the message
     *
     * @return the message
     */
    public String getMessage(){ return this.message; }
}
