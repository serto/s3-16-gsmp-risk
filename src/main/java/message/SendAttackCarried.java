package message;

import scala.Serializable;

/**
 * Message that notify the territory which attacks and the territory which defences
 *
 * @author Matteo Sertori
 */
public class SendAttackCarried implements Serializable {

    private int idTerAttack, idTerDefence, nArmiesToAttacking;

    /**
     * @param idTerAttack The ID of attacking territory
     * @param idTerDefence The ID of defencing territory
     * @param nArmiesToAttacking number of armies
     **/
    public SendAttackCarried(int idTerAttack, int idTerDefence, int nArmiesToAttacking){
        this.idTerAttack = idTerAttack;
        this.idTerDefence = idTerDefence;
        this.nArmiesToAttacking = nArmiesToAttacking;
    }

    /**
     * Return the ID of attacking territory
     *
     * @return The ID of attacking territory
     */
    public int getIdTerAttack(){return this.idTerAttack;}

    /**
     * Return the ID of defencing territory
     *
     * @return The ID of defencing territory
     */
    public int getIdTerDefence(){return this.idTerDefence;}

    /**
     * Return the number of armies
     *
     * @return return the number of armies
     */
    public int getnArmiesToAttacking(){return this.nArmiesToAttacking;}

}
