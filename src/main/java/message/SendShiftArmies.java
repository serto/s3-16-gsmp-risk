package message;

import risk.Territory;
import scala.Serializable;

import java.util.ArrayList;

/**
 * Message who send the armies shifted. This message is send form client to server.
 *
 * @author Matteo Sertori
 **/
public class SendShiftArmies implements Serializable {

    private int shiftFrom, shiftTo;
    private ArrayList<Territory> territories;

    /**
     * @param shiftFrom The id of the territory from where the armies leave
     * @param shiftTo The id of the territory from where the armies arrive
     * @param territories the list of territories
     */
    public SendShiftArmies(int shiftFrom, int shiftTo, ArrayList<Territory> territories){
        this.shiftFrom = shiftFrom;
        this.shiftTo = shiftTo;
        this.territories = territories;
    }

    /**
     * @return return The id of the territory from where the armies leave
     */
    public int getShiftFrom(){return this.shiftFrom; }

    /**
     * @return return The id of the territory from where the armies arrive
     */
    public int getShiftTo(){ return this.shiftTo; }

    /**
     * @return return the list of territories
     */
    public ArrayList<Territory> getTerritories(){ return this.territories;}
}
