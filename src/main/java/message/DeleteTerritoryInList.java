package message;

import scala.Serializable;

/**
 * Message for delete one territory from the list of enemy user. This message is send from the game to client.
 *
 * @author Matteo Sertori
 */
public class DeleteTerritoryInList implements Serializable {

    private int idTer;


    /**
     * @param idTer The ID of Territory
     */
    public DeleteTerritoryInList(int idTer){ this.idTer = idTer; }

    /**
     * Get the ID of Territory
     *
     * @return The ID of Territory
     */
    public int getIdTer() {
        return idTer;
    }
}
