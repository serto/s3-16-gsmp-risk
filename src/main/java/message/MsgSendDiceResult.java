package message;

import scala.Serializable;

/**
 * Message who send the dice results
 *
 * @author Matteo Sertori
 */
public class MsgSendDiceResult implements Serializable {

    private String winner;
    private int winsD, winsA;

    /**
     * @param winner String of the winner
     * @param winsD Numbers of defencing armies lost
     * @param winsA Numbers of attacking armies lost
     */
    public MsgSendDiceResult(String winner, int winsD, int winsA){

        this.winner = winner;
        this.winsA = winsA;
        this.winsD = winsD;
    }


    /**
     * @return the winner
     */
    public String getWinner(){return this.winner;}

    /**
     * @return Get numbers of defencing armies lost
     */
    public int getWinsD(){ return this.winsD;}

    /**
     * @return Get numbers of attacking armies lost
     */
    public int getWinsA(){ return this.winsA;}
}
