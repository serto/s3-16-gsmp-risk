package message;

import scala.Serializable;

/**
 * Message for delete one territory from the list of enemy user. This message is send to Server
 *
 * @author Matteo Sertori
 */
public class MsgDeleteTerritoryInList implements Serializable {

    private int idTer;

    /**
     * @param idTer The Territory ID
     */
    public MsgDeleteTerritoryInList(int idTer){ this.idTer = idTer; }

    /**
     * Return the territory ID to remove from the user list
     *
     * @return the territory ID to remove
     */
    public int getIdTer() {
        return idTer;
    }
}
