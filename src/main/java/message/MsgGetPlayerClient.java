package message;

import scala.Serializable;

/**
 * Message who contains the ID of Player
 *
 * @author Matteo Sertori
 */
public class MsgGetPlayerClient implements Serializable {

    private int playerID;

    /**
     * @param playerID The PlayerID
     */
    public MsgGetPlayerClient(int playerID){
        this.playerID = playerID;
    }

    /**
     * Return Player ID
     *
     * @return playerID
     */
    public int getMessage(){
        return this.playerID;
    }
}
