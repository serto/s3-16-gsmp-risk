package message;

import risk.Territory;
import scala.Serializable;

import java.util.ArrayList;

/**
 * Message who send territories updated
 *
 * @author Matteo Sertori
 */
public class MsgUpdateMap implements Serializable {

    private ArrayList<Territory> territories;

    /**
     * @param territories the list of territories
     */
    public MsgUpdateMap(ArrayList<Territory> territories){
        this.territories = territories;
    }

    /**
     * @return return the list of territories
     */
    public ArrayList<Territory> getMessage(){return this.territories;}
}
