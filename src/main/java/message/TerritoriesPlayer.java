package message;

import scala.Serializable;

import java.util.List;

/**
 * The message send the list of user's player territory and the list of all territory
 *
 * @author Matteo Sertori
 */
public class TerritoriesPlayer implements Serializable {

    private List<?> listPlayer;
    private List<?> listGlobal;


    /**
     * @param listPlayer the list of user's player territory
     * @param listGlobal the list of all territory
     */
    public TerritoriesPlayer(List<?> listPlayer, List<?> listGlobal){
        this.listPlayer = listPlayer;
        this.listGlobal = listGlobal;
    }

    /**
     * @return the list of user's player territory
     */
    public List<?> getPlayerTerritories() {
        return listPlayer;
    }

    /**
     * @return the list of all territory
     */
    public List<?> getGlobalTerritories() {
        return listGlobal;
    }
}
