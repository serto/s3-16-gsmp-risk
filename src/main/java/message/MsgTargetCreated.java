package message;

import risk.target.Target;
import scala.Serializable;

/**
 * Message that send the user's target
 *
 * @author Matteo Sertori
 */
public class MsgTargetCreated implements Serializable {
    private Target target;

    /**
     * @param target The user's target
     */
    public MsgTargetCreated(Target target){
        this.target = target;
    }

    /**
     * Get the user's target
     *
     * @return the user's target
     */
    public Target getTarget(){
        return this.target;
    }
}
