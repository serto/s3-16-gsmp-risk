package esScala


import esScala.DicesManager.DicesManager_Refactored
import risk.Player
import risk.attack.AttackManagerPanel

import scala.collection.mutable.ListBuffer
import scala.collection.JavaConverters._

/**
  * Class to random extraction val to manage the attack and defence dices
  *
  * @author Masi Elisabetta
  */
class ArmiesLost(var nArmiesLost: Int) {
  def increment(): Unit = nArmiesLost = nArmiesLost + 1
}

class NumDices(var nDicesToPlay : Int){
  def nDicesToPlay_(nArmiesToPlay: Int): Unit = if(nArmiesToPlay >= 3) nDicesToPlay = 3 else nDicesToPlay = nArmiesToPlay
}

class ListValueDices(){

  val valDices: ListBuffer[Integer] = ListBuffer[java.lang.Integer]()

  def listValueDices : java.util.List[java.lang.Integer] = valDices.toList.sorted.reverse.asJava

  def extractionValDices(nDices: Int): Unit = {
    for (_ <- 1 to nDices) valDices += 1 + scala.util.Random.nextInt(6)
  }
}

trait DiceManagerTrait{
  def start() : Unit
  def verifyResult() : Unit
  def checkResult : String
}

class DiceManager(nArmiesToAttack : Int, nArmiesToDefence : Int) extends DiceManagerTrait {

  val armiesAttackLost = new ArmiesLost(0)
  val armiesDefenceLost = new ArmiesLost(0)

  val nDicesAttack = new NumDices(0)
  val nDicesDefence = new NumDices(0)

  val valDicesAttack: ListValueDices = new ListValueDices
  val valDicesDefence: ListValueDices = new ListValueDices

  var dicePanel: DicePanel = _


  override def start(): Unit = {
    //println("attack: " + nArmiesToAttack + "defence" + nArmiesToDefence)

    nDicesAttack.nDicesToPlay_(nArmiesToAttack)
    nDicesDefence.nDicesToPlay_(nArmiesToDefence)

    //println("dadi: " + nDicesAttack.nDicesToPlay + nDicesDefence.nDicesToPlay)

    valDicesAttack.extractionValDices(nDicesAttack.nDicesToPlay)
    valDicesDefence.extractionValDices(nDicesDefence.nDicesToPlay)

    //println("dadi A: " + valDicesAttack.listValueDices + "dadi D: " + valDicesDefence.listValueDices)
  }



  override def verifyResult(): Unit = {

    if (valDicesAttack.listValueDices.size <= valDicesDefence.listValueDices.size)
      for (i <- 0 until valDicesAttack.listValueDices.size())
        if (valDicesAttack.listValueDices.get(i) <= valDicesDefence.listValueDices.get(i)) {
          armiesAttackLost.increment()
          dicePanel.updateDicesPanelAfterVerify(i, "DEFENCE")
        } else{
          armiesDefenceLost.increment()
          dicePanel.updateDicesPanelAfterVerify(i, "ATTACK")
        }
    else
      for (i <- 0 until valDicesDefence.listValueDices.size())
        if (valDicesAttack.listValueDices.get(i) <= valDicesDefence.listValueDices.get(i)){
          armiesAttackLost.increment()
          dicePanel.updateDicesPanelAfterVerify(i, "DEFENCE")
        } else{
          armiesDefenceLost.increment()
          dicePanel.updateDicesPanelAfterVerify(i, "ATTACK")
        }


    println("A: " + armiesAttackLost.nArmiesLost + " D: " + armiesDefenceLost.nArmiesLost)

    dicePanel.updateResultPanelAfterVerify(armiesAttackLost.nArmiesLost, armiesDefenceLost.nArmiesLost, checkResult)

  }

  override def checkResult: String = {
    if(armiesAttackLost.nArmiesLost >= armiesDefenceLost.nArmiesLost) " Vince la DIFESA !!"
    else " Vince l' ATTACCO !!"
  }

  def getValDicesAttack: ListValueDices = valDicesAttack
  def getValDicesDefence: ListValueDices = valDicesDefence

  def getNDicesAttack: NumDices = nDicesAttack
  def getNDicesDefence: NumDices = nDicesDefence

  def dicePanel_ (dicePanel_Refactor: DicePanel): Unit = dicePanel = dicePanel_Refactor
}

object DicesManager {
  def apply(attackManagerPanel: AttackManagerPanel, nArmiesToAttack: Int, nArmiesToDefence: Int, playerCurr: Player): DicesManager_Refactored =
    DicesManager_Refactored(attackManagerPanel, nArmiesToAttack, nArmiesToDefence, playerCurr)

  //private
  case class DicesManager_Refactored(attackManagerPanel: AttackManagerPanel, nArmiesToAttack: Int, nArmiesToDefence: Int, playerCurr: Player) {
    val dicesModel: DiceManager = new DiceManager(nArmiesToAttack, nArmiesToDefence)

    dicesModel start()

    val dicePanel = new DicePanel(dicesModel, attackManagerPanel, playerCurr, nArmiesToAttack)

    dicesModel.dicePanel=dicePanel
  }
}


object DiceManagerApp extends App{
  DicesManager(null, 10, 5, null)
}