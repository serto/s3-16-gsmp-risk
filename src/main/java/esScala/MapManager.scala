package esScala

import message.DeleteTerritoryInList
import risk.{Bonus, MapPanel, Player, PlayerPanel}

import scala.collection.mutable.ListBuffer
import scala.collection.JavaConverters._
import scala.language.postfixOps

/**
  * Created by BettaMasi on 12/03/2019.
  */

object State{
  //the game is in the initial reinforce
  val INIT_REINFORCE = 0
  //the game is in the attack turn
  val REINFORCE = 1
  val ATTACKING = 2
  val TERRITORY_DEFENCE = 3
  val TERRITORY_ATTACKING = 4
  val DICES = 5
  //the game is in the shift turn
  val SHIFT_FROM = 6
  val SHIFT_TO = 7
  //the game is in the exchange turn between the player
  val END_TURN = 8
}

class State(var stateCurrent : Int){
  def getState: Int = stateCurrent
  def setState_(newState: Int):Unit = stateCurrent = newState
}

class Territory(var idTerritory: Int){
  def setTerritory(idTerritorySelected: Int): Unit = idTerritory = idTerritorySelected
}

class Jolly(var nJolly: Int) {
  def increment(): Unit = nJolly = nJolly + 1
}

trait MapManagerTrait{
  def getPositionTerritory(idTerritory: Int): java.util.List[java.lang.Integer]
  def distributionArmies(idTerritory: Int): Int
  def setNArmiesToINIT()

  def setNArmiesToReinforce(idTerritory: Int): Int
  def setNArmies_ReinforceTurn() : Unit

  def setTerritoryDefencing(idTerritoryDefence: Int): Unit
  def getArmiesTerritoryDefence():Int
  def setTerritoryAttacking(idTerAttack: Int): Unit
  def updateNArmiesAfterDices(nArmiesForAttack: Int, nArmiesAttackLost: Int, nArmiesDefenceLost: Int)

  def setShift_TerritoryFROM(idTerritoryFrom: Int): Unit
  def setShift_TerritoryTO(idTerritoryTo: Int): Unit
  def updateArmiesTerritory(nArmiesToShift: Int): Unit

  def setFlagConquest(): Unit
  def getBonusList(): java.util.List[java.lang.Integer]
}

class MapManager(mapPanel: MapPanel, player: Player, playerPanel: PlayerPanel) extends MapManagerTrait{

  val state: State = new State(State.INIT_REINFORCE)
  var territoryDefence: Territory = new Territory(-1)
  var territoryAttack: Territory = new Territory(-1)
  var territoryShiftFrom: Territory = new Territory(-1)
  var territoryShiftTo: Territory = new Territory(-1)

  var firstConquest: Boolean = false
  var hasConquered: Boolean = false

  var nJolly: Jolly = new Jolly(0)

  override def getPositionTerritory(idTerritory: Int): java.util.List[Integer] = {
    val coordination: ListBuffer[Integer] = ListBuffer[java.lang.Integer]()

    coordination += mapPanel.getTerritories.get(idTerritory).getX
    coordination += mapPanel.getTerritories.get(idTerritory).getY

    coordination.toList.asJava
  }

  override def distributionArmies(idTerritory: Int): Int = {
    if (player.getnArmiesInit != 0){
      if (checkOwnerTerritories(idTerritory)) {
        mapPanel.addArmiesToTerritory(idTerritory, 1)
        player.setNArmiesInit()
      } else mapPanel.viewMessage("Attenzione non è un tuo territorio")
    }
    if(player.getnArmiesInit == 0){
      player.sendMessage("Press_next")
      playerPanel.getMapPanel.enabledAllButtons(false)
      state.setState_(State.REINFORCE)
    }
    mapPanel.getTerritories.get(idTerritory).getArmies
  }

  def checkOwnerTerritories(idTer: Int): Boolean = {
    if (mapPanel.getTerritories.get(idTer).getPlayerOwner.getPlayerID.equals(player.getPlayerID)) true
    else false
  }

  def setNArmiesToINIT(): Unit = {playerPanel.getPnlInitReinforce.setLblNArmiesTurn(player.getnArmiesInit())}

  override def setNArmiesToReinforce(idTerritory: Int): Int = {
    if(!checkNArmiesPlayerToDistribute()) {
      if (checkOwnerTerritories(idTerritory)) {
        mapPanel.addArmiesToTerritory(idTerritory, 1)
        player.setNArmies()
      } else mapPanel.viewMessage("Attenzione non è un tuo territorio")
    }
    if(checkNArmiesPlayerToDistribute()){
      playerPanel.getPnlAttackManager.getPnlReinforce.setBtnNext()
      state.setState_(State.ATTACKING)
    }

    mapPanel.getTerritories.get(idTerritory).getArmies
  }

  def checkNArmiesPlayerToDistribute(): Boolean = player.getNArmies.equals(0)

  override def setNArmies_ReinforceTurn(): Unit = playerPanel.getPnlAttackManager.getPnlReinforce.setLblNArmiesInThisTurn(player.getNArmies)



  override def setTerritoryDefencing(idTerritoryDefence: Int): Unit = {
    if(!checkTerritoryOwner(idTerritoryDefence, player)){
      playerPanel.getPnlAttackManager.getPnlAttack2.setLblTo(mapPanel.getTerritories.get(idTerritoryDefence).getName)
      territoryDefence.setTerritory(idTerritoryDefence)
      playerPanel.getPnlAttackManager.getPnlAttack2.enableAttackSection()
    } else mapPanel.viewMessage("attenzione devi selezionare un territorio dell'avversario!")
  }

  override def getArmiesTerritoryDefence():Int = mapPanel.getTerritories.get(territoryDefence.idTerritory).getArmies

  override def setTerritoryAttacking(idTerritoryAttack: Int): Unit = {
    if(checkTerritoryOwner(idTerritoryAttack, player)){
      if(checkTerritoryAdjacent(territoryDefence.idTerritory + 1, idTerritoryAttack)){
        if(mapPanel.getTerritories.get(idTerritoryAttack).getArmies > 1){
          playerPanel.getPnlAttackManager.getPnlAttack2.setLblFrom(mapPanel.getTerritories.get(idTerritoryAttack).getName)
          playerPanel.getPnlAttackManager.getPnlAttack2.setLblNArmies(mapPanel.getTerritories.get(idTerritoryAttack).getArmies)
          playerPanel.getPnlAttackManager.getPnlAttack2.enabledBtnAttackPnl(true)
          territoryAttack.setTerritory(idTerritoryAttack)
        }else mapPanel.viewMessage("Attenzione devi selezionare un territorio con almeno 2 armate!")
      }else mapPanel.viewMessage("attenzione devi selezionare un territorio adiacente a quello attaccato!")
    }else mapPanel.viewMessage("attenzione devi selezionare un tuo territorio da cui attaccare!")
  }

  def checkTerritoryOwner(idTerritory: Int, idPlayer: Player): Boolean = {
    mapPanel.getTerritories.get(idTerritory).getPlayerOwner.getPlayerID == idPlayer.getPlayerID
  }

  def checkTerritoryAdjacent(idTerritoryTo: Int, idTerritoryFrom: Int): Boolean = {mapPanel.getTerritories.get(idTerritoryFrom).getAdjacent.contains(idTerritoryTo)}

  override def updateNArmiesAfterDices(nArmiesForAttack: Int, nArmiesAttackLost: Int, nArmiesDefenceLost: Int): Unit = {
    //remove the armies lost in the territories
    mapPanel.removeArmiesToTerritory(territoryAttack.idTerritory, nArmiesAttackLost)
    mapPanel.removeArmiesToTerritory(territoryDefence.idTerritory, nArmiesDefenceLost)

    //the attack player won the defencing territory and set its armies and owner
    if(mapPanel.getTerritories.get(territoryDefence.idTerritory).getArmies <= 0) {
      player.sendMessage(new DeleteTerritoryInList(territoryDefence.idTerritory))
      mapPanel.setPlayerOwnerTerritory(territoryDefence.idTerritory, mapPanel.getTerritories.get(territoryAttack.idTerritory).getPlayerOwner)

      updateNArmiesInTerritories(nArmiesForAttack)

      //check if the player is the winner of the game
      if (player.getTarget.checkTarget(mapPanel.getTerritories.get(territoryAttack.idTerritory).getPlayerOwner)) player.sendMessage("WINNER")

      //manage the distribution of bonus
      hasConquered = true
      if (firstConquest)
        if (player.getBonusList.size() < 7) {
          val bonus: Bonus = new Bonus()
          if (bonus.getBonus == Bonus.JOLLY) {
            nJolly.increment()
            if (nJolly.nJolly <= 2) {
              player.addBonus(bonus.getBonus)
              firstConquest = false
            } else {
              bonus.recalculateBonus()
              player.addBonus(bonus.getBonus)
              firstConquest = false
            }
          } else {
            player.addBonus(bonus.getBonus)
            firstConquest = false
          }
        } else {
          mapPanel.viewMessage("Hai già raggiunto il numero massimo di bonus!")
          this.firstConquest = false
        }
    }
    //update the components in the map panel and defencing panel
    mapPanel.updateNArmiesOfTerritory(territoryAttack.idTerritory, territoryDefence.idTerritory)
    player.sendMessage("Press_next")
  }

  def updateNArmiesInTerritories(nArmiesForAttack: Int): Unit = nArmiesForAttack match {
    case n if n > 3 => mapPanel.setArmiesToTerritory(territoryDefence.idTerritory, 3)
                       mapPanel.removeArmiesToTerritory(territoryAttack.idTerritory, 3)
    case 1 => mapPanel.setArmiesToTerritory(territoryDefence.idTerritory, nArmiesForAttack)
              mapPanel.removeArmiesToTerritory(territoryAttack.idTerritory, nArmiesForAttack)
    case _ => mapPanel.setArmiesToTerritory(territoryDefence.idTerritory, nArmiesForAttack - 1)
              mapPanel.removeArmiesToTerritory(territoryAttack.idTerritory, nArmiesForAttack - 1)
  }

  override def setShift_TerritoryFROM(idTerritoryFrom: Int): Unit = {
    if(checkTerritoryOwner(idTerritoryFrom, player)) {
      playerPanel.getPnlAttackManager.getPnlShift.setLblFrom(mapPanel.getTerritories.get(idTerritoryFrom).getName)
      playerPanel.getPnlAttackManager.getPnlShift.setLblNArmies(mapPanel.getTerritories.get(idTerritoryFrom).getArmies)
      territoryShiftFrom.setTerritory(idTerritoryFrom)
    } else mapPanel.viewMessage("attenzione devi selezionare un tuo territorio!")
  }

  override def setShift_TerritoryTO(idTerritoryTo: Int): Unit = {
    if(checkTerritoryOwner(idTerritoryTo, player))
      if(checkTerritoryAdjacent(idTerritoryTo + 1, territoryShiftFrom.idTerritory)){
        playerPanel.getPnlAttackManager.getPnlShift.setLblTo(mapPanel.getTerritories.get(idTerritoryTo).getName)
        playerPanel.getPnlAttackManager.getPnlShift.enabledBtnShiftPnl(true)
        territoryShiftTo.setTerritory(idTerritoryTo)
      } else mapPanel.viewMessage("attenzione devi selezionare un territorio adiacente a quello selezionato!")
    else mapPanel.viewMessage("attenzione devi selezionare un tuo territorio!")
  }

  override def updateArmiesTerritory(nArmiesToShift: Int): Unit = {
    mapPanel.addArmiesToTerritory(territoryShiftTo.idTerritory, nArmiesToShift)
    mapPanel.removeArmiesToTerritory(territoryShiftFrom.idTerritory, nArmiesToShift)
    mapPanel.updateNArmiesOfTerritory(territoryShiftFrom.idTerritory, territoryShiftTo.idTerritory)
  }

  override def setFlagConquest(): Unit = {
    firstConquest = true
    hasConquered = false
  }

  override def getBonusList(): java.util.List[java.lang.Integer] = {player.getBonusList}
}

/*
public object MapManagerObject{
  def apply(mapPanel: MapPanel, player: Player, playerPanel: PlayerPanel): MapManagerTrait =
    new MapManager(mapPanel, player, playerPanel)
}

*/
