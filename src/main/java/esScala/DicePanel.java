package esScala;

import message.SendDiceResult;
import risk.Bonus;
import risk.Player;
import risk.attack.AttackManagerPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * Class to manage the dices roll.
 * According to the number of armies of attackig and defencing territory, it sets a number of dices with which attack and defence.
 * With the button the player rolls the dices and extracting the random val. They are saved in array which is used to allow the
 * show of face dice image and the verify of final result and how many armies the attack and defence player lost.
 *
 * After the verify of dices, the attacking player can select two new button, which became enable, and he can choose to try again
 * a new attack or ends his attack turn and entry in the shift turn.
 *
 * In the end of turn, if attacking player win a new territory, he receives a bonus as well as the ufficial Risiko game.
 * With a new message dialog panel the player can know what bonus the system sends him with a random extraction.
 * In the new attack turn, the player can choose to play his bonus combination ad distribute a major number of armies during his reinforce turn.
 *
 * Whenever the client sends a new message to the server, which sends the new data to the defencing player to allow him to know
 * the choices of attacking player and update his map.
 *
 * @author Masi Elisabetta
 */
public class DicePanel extends JFrame{

    /* dicesManager   reference to class that manage the dices
       pnlAttackManager reference to class that manages the panel of player and his activities during his turn
       player    reference to class that manages the player and his detail */
    private DiceManager dicesManager;
    private AttackManagerPanel pnlAttackManager;
    private Player player;

    // Components of panel to allow the activities of player
    private JLabel lblResultTitle, lblResultA, lblResultD;
    private JButton btnRoll;

    //array to manage the image of dices according to the array of val of random extracting
    private JLabel[] attack, defense;

    //array to show the result of verify on the val of dices
    private JPanel[] attackVerify, defenseVerify;

    //image of dices side
    private BufferedImage diceAttack, diceAttack1, diceAttack2, diceAttack3, diceAttack4, diceAttack5, diceAttack6,
            diceDefence, diceDefence1, diceDefence2, diceDefence3, diceDefence4, diceDefence5, diceDefence6;

    public DicePanel(DiceManager dicesManagerModel, AttackManagerPanel attackManagerPanel, Player playerCurr, int nArmiesToAttack) {

        this.dicesManager = dicesManagerModel;

        this.pnlAttackManager = attackManagerPanel;
        this.player = playerCurr;


        this.updateImageOfDiceFace();
        this.setDicesViewerInThePanel();

        this.setBounds(100, 100, 357, 428);
        this.setLayout(new BorderLayout());
        this.setVisible(true);

        JPanel mainPanel = new JPanel(new BorderLayout());
        this.add(mainPanel, BorderLayout.CENTER);

        JLabel lblTitle = new JLabel("ESTRAZIONE DADI");
        lblTitle.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 18));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        mainPanel.add(lblTitle, BorderLayout.NORTH);

        JPanel pnlDicesMng = new JPanel(new BorderLayout());
        mainPanel.add(pnlDicesMng, BorderLayout.CENTER);
        this.btnRoll = new JButton("LANCIA I DADI");
        this.btnRoll.setEnabled(true);
        this.btnRoll.addActionListener(e -> {
            btnRoll.setEnabled(false);
            drawDices(dicesManager.getValDicesAttack().listValueDices(), dicesManager.getValDicesDefence().listValueDices());

            dicesManager.verifyResult();
            pnlAttackManager.getMapPanel().getMapManager().updateNArmiesAfterDices(nArmiesToAttack, dicesManager.armiesAttackLost().nArmiesLost(), dicesManager.armiesDefenceLost().nArmiesLost());

        });
        pnlDicesMng.add(btnRoll, BorderLayout.NORTH);

        //create the attack and defence sections with the relative dices
        JPanel pnlDices = new JPanel(new BorderLayout());
        JSeparator separator = new JSeparator();
        pnlDices.add(separator, BorderLayout.NORTH);

        JPanel pnlAttack = new JPanel(new BorderLayout());
        JLabel lblTitleAttack = new JLabel("ATTACCO");
        lblTitleAttack.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitleAttack.setFont(new Font("Arial",  Font.ITALIC, 12));
        pnlAttack.add(lblTitleAttack, BorderLayout.NORTH);

        JPanel pnlDiceA = new JPanel();
        for (int i = 0; i < 3; i++) {
            this.attackVerify[i] = new JPanel();
            this.attack[i] = new JLabel(new ImageIcon(this.diceAttack));
            this.attackVerify[i].add(attack[i]);
            pnlDiceA.add(attackVerify[i]);
        }
        pnlAttack.add(pnlDiceA, BorderLayout.CENTER);
        pnlDices.add(pnlAttack, BorderLayout.CENTER);

        JPanel pnlDefense = new JPanel(new BorderLayout());
        JLabel lblTitleDefense = new JLabel("DIFESA");
        lblTitleDefense.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitleDefense.setFont(new Font("Arial",  Font.ITALIC, 12));
        pnlDefense.add(lblTitleDefense, BorderLayout.NORTH);
        JPanel pnlDiceD = new JPanel();
        for (int i = 0; i < 3; i++) {
            this.defenseVerify[i] = new JPanel();
            this.defense[i] = new JLabel(new ImageIcon(this.diceDefence));
            defenseVerify[i].add(defense[i]);
            pnlDiceD.add(defenseVerify[i]);
        }
        pnlDefense.add(pnlDiceD, BorderLayout.CENTER);
        pnlDices.add(pnlDefense, BorderLayout.SOUTH);
        pnlDicesMng.add(pnlDices, BorderLayout.CENTER);

        //create the section to show the result dices roll
        JPanel pnlResultExtraction = new JPanel(new BorderLayout());
        separator = new JSeparator();
        pnlResultExtraction.add(separator, BorderLayout.NORTH);

        this.lblResultTitle = new JLabel(" Vince ");
        this.lblResultTitle.setFont(new Font("Arial", Font.BOLD, 18));
        this.lblResultTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.lblResultTitle.setForeground(Color.ORANGE);
        pnlResultExtraction.add(lblResultTitle, BorderLayout.CENTER);

        //section to show how many armies the attack and defence lost
        JPanel pnlResultDices = new JPanel(new BorderLayout());
        this.lblResultA = new JLabel(" L'attacco perde: ");
        this.lblResultA.setFont(new Font("Arial",  Font.ITALIC, 12));
        this.lblResultA.setHorizontalAlignment(SwingConstants.CENTER);
        pnlResultDices.add(lblResultA, BorderLayout.CENTER);

        this.lblResultD = new JLabel(" La difesa perde:  ");
        this.lblResultD.setFont(new Font("Arial", Font.ITALIC, 12));
        this.lblResultD.setHorizontalAlignment(SwingConstants.CENTER);
        pnlResultDices.add(lblResultD, BorderLayout.SOUTH);
        pnlResultExtraction.add(pnlResultDices, BorderLayout.SOUTH);
        pnlDicesMng.add(pnlResultExtraction, BorderLayout.SOUTH);

        //section with which the player choose if do a new attack or end is turn
        JPanel pnlButton = new JPanel();
        pnlButton.setLayout(new FlowLayout(FlowLayout.RIGHT));

        JButton btnTryAgain = new JButton("Riprova");
        btnTryAgain.addActionListener(e -> {
            dispose();
            pnlAttackManager.setPnlAttack2();
            player.sendMessage("RIPROVA_ATTACCO");
        });
        pnlButton.add(btnTryAgain);

        JButton btnEndTurn = new JButton("Fine Turno");
        btnEndTurn.addActionListener(e -> {
            //when the player ends his turn, if he has won a territory during the attack, he receives a bonus
            updateBonus();

            dispose();
            pnlAttackManager.setPnlShift();
            player.sendMessage("FINE ATTACCO");
        });
        pnlButton.add(btnEndTurn);
        mainPanel.add(pnlButton, BorderLayout.SOUTH);

        this.setVisible(true);
    }

    /** Method to update the images of dices by the Resources */
    private void updateImageOfDiceFace(){
        try {
            this.diceAttack = ImageIO.read(getClass().getResourceAsStream("/img/diceA0.jpg"));
            this.diceAttack1 = ImageIO.read(getClass().getResourceAsStream("/img/diceA1.jpg"));
            this.diceAttack2 = ImageIO.read(getClass().getResourceAsStream("/img/diceA2.jpg"));
            this.diceAttack3 = ImageIO.read(getClass().getResourceAsStream("/img/diceA3.jpg"));
            this.diceAttack4 = ImageIO.read(getClass().getResourceAsStream("/img/diceA4.jpg"));
            this.diceAttack5 = ImageIO.read(getClass().getResourceAsStream("/img/diceA5.jpg"));
            this.diceAttack6 = ImageIO.read(getClass().getResourceAsStream("/img/diceA6.jpg"));

            this.diceDefence = ImageIO.read(getClass().getResourceAsStream("/img/diceD0.jpg"));
            this.diceDefence1 = ImageIO.read(getClass().getResourceAsStream("/img/diceD1.jpg"));
            this.diceDefence2 = ImageIO.read(getClass().getResourceAsStream("/img/diceD2.jpg"));
            this.diceDefence3 = ImageIO.read(getClass().getResourceAsStream("/img/diceD3.jpg"));
            this.diceDefence4 = ImageIO.read(getClass().getResourceAsStream("/img/diceD4.jpg"));
            this.diceDefence5 = ImageIO.read(getClass().getResourceAsStream("/img/diceD5.jpg"));
            this.diceDefence6 = ImageIO.read(getClass().getResourceAsStream("/img/diceD6.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Method to set the array of label to show the relative dices on the panel */
    private void setDicesViewerInThePanel(){
        this.attack = new JLabel[dicesManager.getNDicesAttack().nDicesToPlay()];
        this.attackVerify = new JPanel[dicesManager.getNDicesAttack().nDicesToPlay()];
        this.defense = new JLabel[dicesManager.getNDicesDefence().nDicesToPlay()];
        this.defenseVerify = new JPanel[dicesManager.getNDicesDefence().nDicesToPlay()];
    }

    /**
     * Method to set the image in the array of dices according to the random val of array valDice
     * @param valDicesAttack  val of attack dices
     *  @param valDicesDefence  val of defence dices
     */
    private void drawDices(List<Integer> valDicesAttack, List<Integer> valDicesDefence){
        for(int position = 0; position < attack.length; position++){
            switch (valDicesAttack.get(position)) {
                case 1:
                    this.attack[position].setIcon(new ImageIcon(this.diceAttack1));
                    break;
                case 2:
                    this.attack[position].setIcon(new ImageIcon(this.diceAttack2));
                    break;
                case 3:
                    this.attack[position].setIcon(new ImageIcon(this.diceAttack3));
                    break;
                case 4:
                    this.attack[position].setIcon(new ImageIcon(this.diceAttack4));
                    break;
                case 5:
                    this.attack[position].setIcon(new ImageIcon(this.diceAttack5));
                    break;
                case 6:
                    this.attack[position].setIcon(new ImageIcon(this.diceAttack6));
                    break;
            }
        }

        for(int position  = 0; position < defense.length; position++){
            switch (valDicesDefence.get(position)) {
                case 1:
                    this.defense[position].setIcon(new ImageIcon(this.diceDefence1));
                    break;
                case 2:
                    this.defense[position].setIcon(new ImageIcon(this.diceDefence2));
                    break;
                case 3:
                    this.defense[position].setIcon(new ImageIcon(this.diceDefence3));
                    break;
                case 4:
                    this.defense[position].setIcon(new ImageIcon(this.diceDefence4));
                    break;
                case 5:
                    this.defense[position].setIcon(new ImageIcon(this.diceDefence5));
                    break;
                case 6:
                    this.defense[position].setIcon(new ImageIcon(this.diceDefence6));
                    break;
            }
        }

    }


    public void updateDicesPanelAfterVerify(int positionDice, String winner){
        switch(winner){
            case "ATTACK":
                attackVerify[positionDice].setBackground(Color.GREEN);
                defenseVerify[positionDice].setBackground(Color.RED);
                break;
            case "DEFENCE":
                attackVerify[positionDice].setBackground(Color.RED);
                defenseVerify[positionDice].setBackground(Color.GREEN);
                break;
        }
    }

    public void updateResultPanelAfterVerify(int nArmiesAttackLost, int nArmiesDefenceLost, String winner){

        this.lblResultTitle.setText("" + winner);
        this.lblResultA.setText("L'attacco perde: " + nArmiesAttackLost + " carrarmati!");
        this.lblResultD.setText("La difesa perde: " + nArmiesDefenceLost + " carrarmati!");

        player.sendMessage(new SendDiceResult(winner, nArmiesAttackLost, nArmiesDefenceLost));
    }

    /**
     * method to show the last bonus obtained from the player during the attack phase and all the bonus owned by the player
     *
     * @autor Genghini Luca
     */
    private void updateBonus(){
        if(pnlAttackManager.getMapPanel().getMapManager().hasConquered()){
            String result = null;
            java.util.List<Integer> tmpList = pnlAttackManager.getMapPanel().getMapManager().getBonusList();
            switch(tmpList.get(tmpList.size()-1)){
                case Bonus.CANNON:
                    result = "Hai ottemuto il bonus CANNONE! Al momento possiedi: \n";
                    break;
                case Bonus.INFANTRYMAN:
                    result = "Hai ottemuto il bonus FANTE! Al momento possiedi: \n";
                    break;
                case Bonus.HORSE:
                    result = "Hai ottemuto il bonus CAVALLO! Al momento possiedi: \n";
                    break;
                case Bonus.JOLLY:
                    result = "Hai ottemuto il bonus JOLLY! Al momento possiedi: \n";
                    break;
            }
            for(int bonus : tmpList){
                switch(bonus){
                    case Bonus.CANNON:
                        result = result + " CANNONE";
                        break;
                    case Bonus.INFANTRYMAN:
                        result = result + " FANTE";
                        break;
                    case Bonus.HORSE:
                        result = result + " CAVALLO";
                        break;
                    case Bonus.JOLLY:
                        result = result + " JOLLY";;
                        break;
                }
            }
            JOptionPane.showMessageDialog(null, result);
        }
    }

}