package risk;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Class to create the place of game with player's panel to control all his operations during his turn
 * and a panel for the game's table, a world map table as well as the official Risiko game.
 * When the loading is completed the system sends a new message to allow the start of the entire game,
 * so the show of frame for each player.
 *
 * @author Masi Elisabetta
 */

public class RiskGUI extends JFrame {

    /* mapPanel     reference to class that manages the panel of world map game table
    *  pnlPlayer    reference to class that manages the panel of player and his activities during his turn*/
    private MapPanel mapPanel;
    private PlayerPanel playerPanel;

    public RiskGUI(ArrayList<Territory> territories, Player player) {

        initComponents(territories, player);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("gsmp-risk Game");
        this.setBackground(Color.DARK_GRAY);
        this.pack();     //it will automatically change the size of the frames according to the size of components in it.
        this.setVisible(true);
    }

    /**
     * Method to set the structure of frame with a section for the player and his panel, and a section
     * to show the map panel with the world map and a button for each territory.
     * When the frame is loading, the client sends a new message to server to update the state of game
     * and to allow the show of its some panel
     *
     * @param territories   array of territories in the world map
     * @param player        player current
     */
    private void initComponents(ArrayList<Territory> territories, Player player) {

        this.setLayout(new BorderLayout());
        this.setBackground(Color.DARK_GRAY);

        playerPanel = new PlayerPanel(player);
        this.add(playerPanel, BorderLayout.WEST);

        mapPanel = new MapPanel(territories, playerPanel, player);
        mapPanel.setBackground(Color.DARK_GRAY);
        this.add(mapPanel, BorderLayout.CENTER);

        playerPanel.setMapPanelReference(mapPanel);

        player.setGame(this);
        player.sendMessage("LOADING_COMPLETED");
    }

    /**
     * Method to get the reference to the player section in the general frame of game
     *
     * @return reference to player section in the frame
     */
    public PlayerPanel getPlayerPanel(){return this.playerPanel;}

    /**
     * Method to get the reference to the map section in the general frame of game
     *
     * @return  reference to the map section in the general frame of game
     */
    public MapPanel getMapPanel(){return this.mapPanel;}


}
