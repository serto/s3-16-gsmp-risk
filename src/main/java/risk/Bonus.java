package risk;

import java.util.Random;

/**
 * Class to manage different kinds of bonus of the game.
 *
 * @author Genghini Luca
 */

public class Bonus {

    //id of the different bonus
    public static final int CANNON = 0;
    public static final int INFANTRYMAN = 1;
    public static final int HORSE = 2;
    public static final int JOLLY = 3;

    //number of armies for the different kinds of bonus
    public static final int ARMIES_BONUS_CANNON = 4;
    public static final int ARMIES_BONUS_INFANTRYMAN = 6;
    public static final int ARMIES_BONUS_HORSE = 8;
    public static final int ARMIES_BONUS_ONEFORTYPE = 10;
    public static final int ARMIES_BONUS_JOLLY = 12;

    //id of the specific bonus
    private int bonus;


    /**
     * constructor of the class which assigns randomly between the 4 possible bonus id
     */
    public Bonus(){
        this.bonus = new Random().nextInt(4);
    }


    /**
     * method which recalculates the bonus id
     */
    public void recalculateBonus(){
        this.bonus = new Random().nextInt(3);
    }

    /**
     * @return the bonus id
     */
    public int getBonus(){
        return this.bonus;
    }
}
