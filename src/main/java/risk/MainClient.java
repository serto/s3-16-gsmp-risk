package risk;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import client.Client;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * This class create the client GUI to connect with the server
 *
 * @author Matteo Sertori
 */
public class MainClient extends java.awt.Frame {

    private JButton startButton, exitButton;
    private JLabel jLabel, lblIP;
    private JPanel jPanel, pnlIP;
    private JTextField ipServer;
    private Image logo;

    private String myIP;

    public MainClient(){
        initComponents();

    }

    private void initComponents() {
        startButton = new JButton();
        exitButton = new JButton();
        jLabel = new JLabel();
        jPanel = new JPanel();

        this.pnlIP = new JPanel(new BorderLayout());
        this.pnlIP.setBackground(new Color(1, 1, 1));
        this.lblIP = new JLabel("IP Server:");
        this.lblIP.setForeground(Color.YELLOW);
        this.lblIP.setHorizontalAlignment(SwingConstants.LEFT);
        this.pnlIP.add(lblIP, BorderLayout.WEST);
        try {
            ipServer = new JTextField(InetAddress.getLocalHost().getHostAddress());
            this.ipServer.setHorizontalAlignment(SwingConstants.CENTER);
            this.pnlIP.add(ipServer, BorderLayout.CENTER);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        setBackground(new Color(1, 1, 1));

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                System.exit(0);
            }
        });

        jPanel.setBackground(new Color(1, 1, 1));
        jPanel.setName("jPanel");

        startButton.setText("Start Game");
        startButton.setName("startButton");
        startButton.addActionListener(event -> {

            try {
                myIP = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            Config config = ConfigFactory.parseString(
                " akka { \n" +
                        " actor { \n" +
                        " provider = remote\n" +
                        " enable-additional-serialization-bindings = on \n" +
                        " akka.actor.allow-java-serialization = off \n" +
                        "}\n" +
                        " remote { \n" +
                        " enabled-transports = [\"akka.remote.netty.tcp\"]\n" +
                        " netty.tcp { \n" +
                        " hostname = \"" + myIP +"\"\n" +
                        " port = 0\n" +
                        "}\n" +
                        "}\n" +
                        "}\n");

            ActorSystem system = ActorSystem.apply("ClientSystem", config);

            ActorRef actorClient = system.actorOf(Props.create(Client.class, ipServer.getText()), "ActorClient");

            this.setVisible(false);

        });

        exitButton.setText("Exit");
        exitButton.setName("exitButton");
        exitButton.addActionListener(event -> System.exit(0));

        try {
            this.logo = ImageIO.read(getClass().getResourceAsStream("/img/logoRiskGSMP.png")).getScaledInstance(350,120, 2);
        } catch (IOException e) {
            e.printStackTrace();
        }

        jLabel.setIcon(new ImageIcon(this.logo));

        jLabel.setName("jLabel");

        GroupLayout jPanelLayout = new GroupLayout(jPanel);
        jPanel.setLayout(jPanelLayout);
        jPanelLayout.setHorizontalGroup(
                jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, jPanelLayout.createSequentialGroup()
                                .addGap(91, 91, 91)
                                .addGroup(jPanelLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                        .addComponent(pnlIP, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                                        .addComponent(startButton, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                                        .addComponent(exitButton, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
                                .addGap(87, 87, 87))
        );
        jPanelLayout.setVerticalGroup(
                jPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel)
                                .addGap(34, 34, 34)
                                .addComponent(pnlIP)
                                .addGap(12, 12, 12)
                                .addComponent(startButton)
                                .addGap(12, 12, 12)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(exitButton)
                                .addContainerGap(42, Short.MAX_VALUE))
        );

        add(jPanel, BorderLayout.CENTER);
        setBounds(500, 350, 320, 300);
        pack();
    }

    public static void main(String args[]) {
        EventQueue.invokeLater(() -> new MainClient().setVisible(true));
    }
}
