package risk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * This class manages the frame to show the result of the game.
 * According the input param, it sets its labels with the result: winner or loser, and shows the relative image.
 * In this way, the player knows if he is the winner or loser.
 *
 * @author Elisabetta Masi
 */

public class ResultGamePanel extends JFrame {

    public ResultGamePanel(String result) {

        this.setBounds(600, 350, 720, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JFrame frameResult = new JFrame();
        frameResult.setBounds(500, 400, 300, 200);

        JPanel pnlResult = new JPanel(new BorderLayout());
        pnlResult.setBackground(Color.BLACK);
        frameResult.add(pnlResult);

        JLabel lblResultTitle = new JLabel();
        lblResultTitle.setFont(new Font("COMIC SANS MS",  Font.BOLD, 20));
        lblResultTitle.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel lblResultTitle1 = new JLabel();
        lblResultTitle1.setFont(new Font("COMIC SANS MS",  Font.BOLD, 20));
        lblResultTitle1.setHorizontalAlignment(SwingConstants.CENTER);

        JButton btnOK = new JButton("OK");
        btnOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });


        pnlResult.add(lblResultTitle, BorderLayout.NORTH);
        pnlResult.add(lblResultTitle1, BorderLayout.CENTER);
        pnlResult.add(btnOK, BorderLayout.SOUTH);


        JLabel imageLabel = new JLabel();
        try{
            if(result.equals("WINNER")){
                ImageIcon imgWinner = new ImageIcon(this.getClass().getResource("/img/winner.gif"));
                imageLabel.setIcon(imgWinner);
                imageLabel.setHorizontalAlignment(SwingConstants.CENTER);

                lblResultTitle.setText("CONGRATULATION!");
                lblResultTitle1.setText("YOU ARE THE WINNER!!!");
            }else {
                lblResultTitle.setText("I'M SO SORRY!");
                lblResultTitle1.setText("YOU ARE LOSER!!!");
                imageLabel.setHorizontalAlignment(SwingConstants.CENTER);
                imageLabel.setFont(new Font("Arial",  Font.ITALIC, 18));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        this.add(imageLabel);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        frameResult.setVisible(true);
    }
}


