package risk;

/**
 * Interface to manage the different panels which compose the player panel, one for every round of the player turn.
 *
 * @author Masi Elisabetta
 */
public interface StateGamePanel {

    /**
     * method to enable the panel when the player ends the previous activity
     */
    void setPanelComponent_ON();


    /**
     * method to reset the component of panel when start a new turn
     */
    void resetComponent();

    /**
     * method to disable the panel when the player ends this activity
     */
    void setPanelComponent_OFF();

}