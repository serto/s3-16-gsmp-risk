package risk;

import message.SendDiceResult;
import risk.attack.AttackManagerPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Class to manage the dices roll.
 * According to the number of armies of attackig and defencing territory, it sets a number of dices with which attack and defence.
 * With the button the player rolls the dices and extracting the random val. They are saved in array which is used to allow the
 * show of face dice image and the verify of final result and how many armies the attack and defence player lost.
 *
 * After the verify of dices, the attacking player can select two new button, which became enable, and he can choose to try again
 * a new attack or ends his attack turn and entry in the shift turn.
 *
 * In the end of turn, if attacking player win a new territory, he receives a bonus as well as the ufficial Risiko game.
 * With a new message dialog panel the player can know what bonus the system sends him with a random extraction.
 * In the new attack turn, the player can choose to play his bonus combination ad distribute a major number of armies during his reinforce turn.
 *
 * Whenever the client sends a new message to the server, which sends the new data to the defencing player to allow him to know
 * the choices of attacking player and update his map.
 *
 * @author Masi Elisabetta
 */
public class DicePanelOld extends JFrame{

    /* diceMng   reference to class that manage the random extraction of val for the dices
       pnlAttackManager reference to class that manages the panel of player and his activities during his turn
       player    reference to class that manages the player and his detail */
    private DiceManagerOld diceMng;
    private AttackManagerPanel pnlAttackManager;
    private Player player;

    // Components of panel to allow the activities of player
    private JLabel lblResultTitle, lblResultA, lblResultD;
    private JButton btnRoll;

    //array to manage the image of dices according to the array of val of random extracting
    private JLabel[] attack, defense;

    //array to show the result of verify on the val of dices
    private JPanel[] attackVerify, defenseVerify;

    //array for the val of random extracting
    private int[] valDicesA = new int[4] , valDicesD = new int[4];

    //number of attack and defence dices according to the number of armies
    private int nDicesA, nDicesD;

    //number of armies lost by attack and defence after the dice roll
    private int nArmiesDefenceLost = 0, nArmiesAttackLost = 0;

    //image of dices side
    private BufferedImage diceA, diceA1, diceA2, diceA3, diceA4, diceA5, diceA6,
            diceD, diceD1, diceD2, diceD3, diceD4, diceD5, diceD6;

    /**
     * @param attackManagerPanel        reference to class that manages the panel of player and his activities during his turn
     * @param nArmiesToAttack    number of armies with which the player chooses to attack
     * @param nArmiesToDefence   number of armies of defencing territory
     * @param playerCurr         reference to class that manages the player and his detail
     */
    public DicePanelOld(AttackManagerPanel attackManagerPanel, int nArmiesToAttack, int nArmiesToDefence, Player playerCurr) {

        this.diceMng = new DiceManagerOld();
        this.pnlAttackManager = attackManagerPanel;
        this.player = playerCurr;

        this.setNDices(nArmiesToAttack, nArmiesToDefence);
        this.updateImage();

        this.setBounds(100, 100, 357, 428);
        this.setLayout(new BorderLayout());
        this.setVisible(true);

        JPanel mainPanel = new JPanel(new BorderLayout());
        this.add(mainPanel, BorderLayout.CENTER);

        JLabel lblTitle = new JLabel("ESTRAZIONE DADI");
        lblTitle.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 18));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        mainPanel.add(lblTitle, BorderLayout.NORTH);

        JPanel pnlDicesMng = new JPanel(new BorderLayout());
        mainPanel.add(pnlDicesMng, BorderLayout.CENTER);
        this.btnRoll = new JButton("LANCIA I DADI");
        this.btnRoll.setEnabled(true);
        this.btnRoll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRoll.setEnabled(false);
                valDicesA = diceMng.extractionValDices(nDicesA);
                valDicesD = diceMng.extractionValDices(nDicesD);

                drawDices(valDicesA, valDicesD);
                verifyResult(valDicesA, valDicesD, nArmiesToAttack);
            }
        });
        pnlDicesMng.add(btnRoll, BorderLayout.NORTH);

        //create the attack and defence sections with the relative dices
        JPanel pnlDices = new JPanel(new BorderLayout());
        JSeparator separator = new JSeparator();
        pnlDices.add(separator, BorderLayout.NORTH);

        JPanel pnlAttack = new JPanel(new BorderLayout());
        JLabel lblTitleAttack = new JLabel("ATTACCO");
        lblTitleAttack.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitleAttack.setFont(new Font("Arial",  Font.ITALIC, 12));
        pnlAttack.add(lblTitleAttack, BorderLayout.NORTH);

        JPanel pnlDiceA = new JPanel();
        for (int i = 0; i < nDicesA; i++) {
            this.attackVerify[i] = new JPanel();
            this.attack[i] = new JLabel(new ImageIcon(this.diceA));
            this.attackVerify[i].add(attack[i]);
            pnlDiceA.add(attackVerify[i]);
        }
        pnlAttack.add(pnlDiceA, BorderLayout.CENTER);
        pnlDices.add(pnlAttack, BorderLayout.CENTER);

        JPanel pnlDefense = new JPanel(new BorderLayout());
        JLabel lblTitleDefense = new JLabel("DIFESA");
        lblTitleDefense.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitleDefense.setFont(new Font("Arial",  Font.ITALIC, 12));
        pnlDefense.add(lblTitleDefense, BorderLayout.NORTH);
        JPanel pnlDiceD = new JPanel();
        for (int i = 0; i < nDicesD; i++) {
            this.defenseVerify[i] = new JPanel();
            this.defense[i] = new JLabel(new ImageIcon(this.diceD));
            defenseVerify[i].add(defense[i]);
            pnlDiceD.add(defenseVerify[i]);
        }
        pnlDefense.add(pnlDiceD, BorderLayout.CENTER);
        pnlDices.add(pnlDefense, BorderLayout.SOUTH);
        pnlDicesMng.add(pnlDices, BorderLayout.CENTER);

        //create the section to show the result dices roll
        JPanel pnlResultExtraction = new JPanel(new BorderLayout());
        separator = new JSeparator();
        pnlResultExtraction.add(separator, BorderLayout.NORTH);

        this.lblResultTitle = new JLabel(" Vince ");
        this.lblResultTitle.setFont(new Font("Arial", Font.BOLD, 18));
        this.lblResultTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.lblResultTitle.setForeground(Color.ORANGE);
        pnlResultExtraction.add(lblResultTitle, BorderLayout.CENTER);

        //section to show how many armies the attack and defence lost
        JPanel pnlResultDices = new JPanel(new BorderLayout());
        this.lblResultA = new JLabel(" L'attacco perde: ");
        this.lblResultA.setFont(new Font("Arial",  Font.ITALIC, 12));
        this.lblResultA.setHorizontalAlignment(SwingConstants.CENTER);
        pnlResultDices.add(lblResultA, BorderLayout.CENTER);

        this.lblResultD = new JLabel(" La difesa perde:  ");
        this.lblResultD.setFont(new Font("Arial", Font.ITALIC, 12));
        this.lblResultD.setHorizontalAlignment(SwingConstants.CENTER);
        pnlResultDices.add(lblResultD, BorderLayout.SOUTH);
        pnlResultExtraction.add(pnlResultDices, BorderLayout.SOUTH);
        pnlDicesMng.add(pnlResultExtraction, BorderLayout.SOUTH);

        //section with which the player choose if do a new attack or end is turn
        JPanel pnlButton = new JPanel();
        pnlButton.setLayout(new FlowLayout(FlowLayout.RIGHT));

        JButton btnTryAgain = new JButton("Riprova");
        btnTryAgain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                pnlAttackManager.setPnlAttack2();
                player.sendMessage("RIPROVA_ATTACCO");
            }
        });
        pnlButton.add(btnTryAgain);

        JButton btnEndTurn = new JButton("Fine Turno");
        btnEndTurn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //when the player ends his turn, if he has won a territory during the attack, he receives a bonus
                updateBonus();

                dispose();
                pnlAttackManager.setPnlShift();
                player.sendMessage("FINE ATTACCO");
            }
        });
        pnlButton.add(btnEndTurn);
        mainPanel.add(pnlButton, BorderLayout.SOUTH);
    }

    /**
     * Method to update the images of dices by the Resources
     */
    private void updateImage(){
        try {
            this.diceA = ImageIO.read(getClass().getResourceAsStream("/img/diceA0.jpg"));
            this.diceA1 = ImageIO.read(getClass().getResourceAsStream("/img/diceA1.jpg"));
            this.diceA2 = ImageIO.read(getClass().getResourceAsStream("/img/diceA2.jpg"));
            this.diceA3 = ImageIO.read(getClass().getResourceAsStream("/img/diceA3.jpg"));
            this.diceA4 = ImageIO.read(getClass().getResourceAsStream("/img/diceA4.jpg"));
            this.diceA5 = ImageIO.read(getClass().getResourceAsStream("/img/diceA5.jpg"));
            this.diceA6 = ImageIO.read(getClass().getResourceAsStream("/img/diceA6.jpg"));

            this.diceD = ImageIO.read(getClass().getResourceAsStream("/img/diceD0.jpg"));
            this.diceD1 = ImageIO.read(getClass().getResourceAsStream("/img/diceD1.jpg"));
            this.diceD2 = ImageIO.read(getClass().getResourceAsStream("/img/diceD2.jpg"));
            this.diceD3 = ImageIO.read(getClass().getResourceAsStream("/img/diceD3.jpg"));
            this.diceD4 = ImageIO.read(getClass().getResourceAsStream("/img/diceD4.jpg"));
            this.diceD5 = ImageIO.read(getClass().getResourceAsStream("/img/diceD5.jpg"));
            this.diceD6 = ImageIO.read(getClass().getResourceAsStream("/img/diceD6.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Method to set the number of dices according the number of territory armies selected
     *
     * @param nArmiesToAttack   number of armies with which the player chooses to attack
     * @param nArmiesToDefence  number of armies of defencing territory
     */
    private void setNDices(int nArmiesToAttack, int nArmiesToDefence){
        if(nArmiesToAttack >= 3){
            this.nDicesA = 3;
        }else{
            this.nDicesA = nArmiesToAttack;
        }

        if(nArmiesToDefence >= 3){
            this.nDicesD = 3;
        }else{
            this.nDicesD = nArmiesToDefence;
        }

        //set also the array of label to show the relative dices on the panel
        this.attack = new JLabel[nDicesA];
        this.attackVerify = new JPanel[nDicesA];
        this.defense = new JLabel[nDicesD];
        this.defenseVerify = new JPanel[nDicesD];
    }

    /**
     * Method to set the image in the array of dices according to the random val of array valDice
     *
     * @param valDiceA  val of attack dices
     * @param valDiceD  val of defence dices
     */
    private void drawDices(int[] valDiceA, int[] valDiceD){
        for(int i = 0; i < attack.length; i++){
            switch (valDiceA[i]) {
                case 1:
                    this.attack[i].setIcon(new ImageIcon(this.diceA1));
                    break;
                case 2:
                    this.attack[i].setIcon(new ImageIcon(this.diceA2));
                    break;
                case 3:
                    this.attack[i].setIcon(new ImageIcon(this.diceA3));
                    break;
                case 4:
                    this.attack[i].setIcon(new ImageIcon(this.diceA4));
                    break;
                case 5:
                    this.attack[i].setIcon(new ImageIcon(this.diceA5));
                    break;
                case 6:
                    this.attack[i].setIcon(new ImageIcon(this.diceA6));
                    break;
            }
        }

        for(int i = 0; i < defense.length; i++){
            switch (valDiceD[i]) {
                case 1:
                    this.defense[i].setIcon(new ImageIcon(this.diceD1));
                    break;
                case 2:
                    this.defense[i].setIcon(new ImageIcon(this.diceD2));
                    break;
                case 3:
                    this.defense[i].setIcon(new ImageIcon(this.diceD3));
                    break;
                case 4:
                    this.defense[i].setIcon(new ImageIcon(this.diceD4));
                    break;
                case 5:
                    this.defense[i].setIcon(new ImageIcon(this.diceD5));
                    break;
                case 6:
                    this.defense[i].setIcon(new ImageIcon(this.diceD6));
                    break;
            }
        }

    }

    /**
     * Method to compare the val of dices and verify who is the winner and how many armies the attack and defence lose.
     *
     * @param valDicesA        valor of defence dices
     * @param valDicesD        valor of attack dices
     * @param nArmiesToAttack  number of armies with which the player chose to attack
     */
    private void verifyResult(int[] valDicesA, int[] valDicesD, int nArmiesToAttack){
        if(nDicesA <= nDicesD) {
            for (int i = 0; i < nDicesA; i++) {
                if (valDicesA[i] <= valDicesD[i]) {
                    attackVerify[i].setBackground(Color.RED);
                    defenseVerify[i].setBackground(Color.GREEN);
                    nArmiesAttackLost++;
                } else {
                    attackVerify[i].setBackground(Color.GREEN);
                    defenseVerify[i].setBackground(Color.RED);
                    nArmiesDefenceLost++;
                }
            }
        }else{
            for (int i = 0; i < nDicesD; i++) {
                if (valDicesA[i] <= valDicesD[i]) {
                    attackVerify[i].setBackground(Color.RED);
                    defenseVerify[i].setBackground(Color.GREEN);
                    nArmiesAttackLost++;
                } else {
                    attackVerify[i].setBackground(Color.GREEN);
                    defenseVerify[i].setBackground(Color.RED);
                    nArmiesDefenceLost++;
                }
            }
        }

        if(nArmiesDefenceLost <= nArmiesAttackLost){
            this.lblResultTitle.setText(" Vince la DIFESA !!");
            player.sendMessage(new SendDiceResult("DIFESA", nArmiesAttackLost, nArmiesDefenceLost));
        }else{
            this.lblResultTitle.setText(" Vince l' ATTACCO !!");
            player.sendMessage(new SendDiceResult("ATTACCO", nArmiesAttackLost, nArmiesDefenceLost));
        }

        pnlAttackManager.getMapPanel().getMapManager().updateNArmiesAfterDices(nArmiesToAttack, nArmiesAttackLost, nArmiesDefenceLost);

        this.lblResultA.setText("L'attacco perde: " + nArmiesAttackLost + " carrarmati!");
        this.lblResultD.setText("La difesa perde: " + nArmiesDefenceLost + " carrarmati!");
    }

    /**
     * method to show the last bonus obtained from the player during the attack phase and all the bonus owned by the player
     *
     * @autor Genghini Luca
     */
    private void updateBonus(){
        if(pnlAttackManager.getMapPanel().getMapManager().hasConquered()){
            String result = null;
            java.util.List<Integer> tmpList = pnlAttackManager.getMapPanel().getMapManager().getBonusList();
            switch(tmpList.get(tmpList.size()-1)){
                case Bonus.CANNON:
                    result = "Hai ottemuto il bonus CANNONE! Al momento possiedi: \n";
                    break;
                case Bonus.INFANTRYMAN:
                    result = "Hai ottemuto il bonus FANTE! Al momento possiedi: \n";
                    break;
                case Bonus.HORSE:
                    result = "Hai ottemuto il bonus CAVALLO! Al momento possiedi: \n";
                    break;
                case Bonus.JOLLY:
                    result = "Hai ottemuto il bonus JOLLY! Al momento possiedi: \n";
                    break;
            }
            for(int bonus : tmpList){
                switch(bonus){
                    case Bonus.CANNON:
                        result = result + " CANNONE";
                        break;
                    case Bonus.INFANTRYMAN:
                        result = result + " FANTE";
                        break;
                    case Bonus.HORSE:
                        result = result + " CAVALLO";
                        break;
                    case Bonus.JOLLY:
                        result = result + " JOLLY";;
                        break;
                }
            }
            JOptionPane.showMessageDialog(null, result);
        }
    }
}