package risk;

import scala.Serializable;

import java.util.ArrayList;

/**
 * Class for management all the aspects of a territory.
 * This class implements Serializable because it will be sent into a message by the system.
 *
 * @author Genghini Luca
 */
public class Territory implements Serializable {

    private int corX, corY, armies;
    private String nameTerritory;

    private Player playerOwner;
    private ArrayList<Integer> adjacents;

    /**
     * @param name the name of the territory
     * @param adjacents the list of the adjacent territories
     * @param corX the X cordinate for setting the inherent button into the map
     * @param corY the Y cordinate for setting the inherent button into the map
     * @param owner the player who owns the territory
     */
    public Territory(String name, ArrayList<Integer> adjacents, int corX, int corY, Player owner){
        this.nameTerritory = name;
        this.corX = corX;
        this.corY = corY;
        this.adjacents = adjacents;
        this.playerOwner = owner;
        this.armies = 0;
    }

    /**
     * method to get name of the territory
     *
     * @return the name of the territory
     */
    public String getName(){
        return nameTerritory;
    }

    /**
     * method to get the X cordinate of the territory
     *
     * @return the X cordinate of the territory
     */
    public int getX(){
        return corX;
    }

    /**
     * method to get the Y cordinate of the territory
     *
     * @return the Y cordinate of the territory
     */
    public int getY(){
        return corY;
    }

    /**
     * method to get the list of the adjacent territories of the territory
     *
     * @return the list of the adjacent territories of the territory
     */
    public ArrayList<Integer> getAdjacent(){return this.adjacents;}

    /**
     * method to get the number of armies of the territory
     *
     * @return the number of armies of the territory
     */
    public int getArmies(){
        return armies;
    }

    /**
     * method for adding armies to the territory
     *
     * @param numArmiesAdd the number of the armies to add at the territory
     */
    public void addArmies(int numArmiesAdd){
        this.armies += numArmiesAdd;
    }

    /**
     * method for subtracting armies to the territory
     *
     * @param numArmiesLess the number of the armies to subtract at the territory
     */
    public void lessArmies(int numArmiesLess){ this.armies -= numArmiesLess; }

    /**
     * method for setting the armies of the territory
     *
     * @param numArmies number of armies to set into the territory
     */
    public void setNArmies(int numArmies){this.armies = numArmies;}

    /**
     * method for setting the new player who owns the territory
     *
     * @param newPlayer the new player who owns the territory
     */
    public void setPlayerOwner(Player newPlayer){
        this.playerOwner = newPlayer;
    }

    /**
     * method to get the player who owns the territory
     *
     * @return the player who owns the territory
     */
    public Player getPlayerOwner() {return this.playerOwner; }
}