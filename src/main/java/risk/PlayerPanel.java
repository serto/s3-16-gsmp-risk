package risk;

import risk.attack.*;
import risk.defence.*;

import javax.swing.*;
import java.awt.*;

/**
 * Class to create and manager the section of player's control operations in the different turn
 *
 * In particular the class is managed by some state of the game.
 * The class create a general command panel with a panel for each state of game and according to the state,
 * the general panel shows the component of init reinforce, attack or defence. Then, to allow a better menagement,
 * the class implements some component for attack and defence commands creating two new subclasses as panel: AttackManagerPanel
 * and DefenceManagerPanel. Everyone implements its components and allow the commands of some data,
 * acccording to the game state of the player.
 *
 * In the end, the class set panel as first state: INIT_REINFORCE, as well as the game state so the map panel.
 *
 * @author Masi Elisabetta
 */

public class PlayerPanel extends JPanel{

    //State of general player panel according to his game state
    private static final int INIT_REINFORCE = 0;
    public static final int ATTACK = 1;
    public static final int DEFENCE = 2;

    /*mapPanel reference to class that manages the panel of game map
    * player    reference to class that manages the player and his detail */
    private MapPanel mapPanel;
    private Player player;

    /*pnlInitReinforce      panel to show the component for the init reinforce turn
    * pnlCommandAttack      panel to show the component for the attack turn
    * pnlCommandDefence     panel to show the component for the defence turn*/
    private InitialReinforcePanel pnlInitReinforce;
    private AttackManagerPanel pnlCommandAttack;
    private DefenceManagerPanel  pnlCommandDefence;

    // Components of panel to allow the activities of player
    private JPanel pnlCommand;
    private JLabel lblTitle;

    /**
     * @param playerCurr   reference to player and his detail
     */
    public PlayerPanel(Player playerCurr) {
        this.player = playerCurr;

        this.setPreferredSize(new Dimension(240, 480));
        this.setLayout(new BorderLayout());
        this.setBackground(Color.DARK_GRAY);

        this.initComponents();
    }

    /**
     * Method to create and manage all component of panel and sub-panel
     */
    public void initComponents(){

        JPanel pnlTitle = new JPanel(new BorderLayout());
        pnlTitle.setBackground(Color.DARK_GRAY);
        lblTitle = new JLabel("Utente : " + player.getPlayerID());
        lblTitle.setForeground(Color.WHITE);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 18));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        pnlTitle.add(lblTitle, BorderLayout.NORTH);

        //create a section to show the target of current player
        JTextArea lblTarget = new JTextArea();
        lblTarget.append(" " + player.getTarget().getDescr());
        lblTarget.setWrapStyleWord(true);
        lblTarget.setLineWrap(true);
        lblTarget.setAlignmentX(CENTER_ALIGNMENT);
        lblTarget.setEditable(false);
        lblTarget.setFont(new Font("Arial", Font.ITALIC, 14 ));
        lblTarget.setForeground(Color.WHITE);
        lblTarget.setBackground(Color.DARK_GRAY);

        pnlTitle.add(lblTarget, BorderLayout.CENTER);
        this.add(pnlTitle, BorderLayout.NORTH);

        //create a general command panel with a panel for each state of game
        pnlCommand = new JPanel(new BorderLayout());
        pnlCommand.setPreferredSize(new Dimension(120, 30));
        pnlCommand.setBackground(Color.DARK_GRAY);

        pnlInitReinforce = new InitialReinforcePanel(this, player);
        this.pnlCommand.add(pnlInitReinforce, BorderLayout.NORTH);

        pnlCommandAttack = new AttackManagerPanel(this, this.player);
        this.pnlCommand.add(pnlCommandAttack, BorderLayout.CENTER);

        pnlCommandDefence = new DefenceManagerPanel();
        this.pnlCommand.add(pnlCommandDefence, BorderLayout.CENTER);

        this.add(pnlCommand, BorderLayout.CENTER);

        this.setStateGame(INIT_REINFORCE);
    }

    /**
     * Method to set a new state of player panel.
     * When the state changes the old state panel and its components are reset and removed by the
     * general panel. Than the general panel is upgraded with the new state panel and its components.
     * Before to set visible the new player panel for current turn, the method reset the old component
     * to avoid some errors.
     * Whereupon, during the attack turn, the method must check some possible new bonus and new territories conquest,
     * so set a new number of player armies for the current turn.
     *
     * @param newState  new state of panel
     */
    public void setStateGame(int newState){
        switch (newState) {
            case INIT_REINFORCE:
                this.pnlInitReinforce.setVisible(true);
                break;

            case ATTACK:
                this.lblTitle.setText("Utente : " + player.getPlayerID() + " Attacca");
                this.pnlInitReinforce.setVisible(false);

                BorderLayout layout = (BorderLayout)pnlCommand.getLayout();
                this.pnlCommand.remove(layout.getLayoutComponent(BorderLayout.CENTER));
                this.pnlCommand.add(pnlCommandAttack, BorderLayout.CENTER);

                this.pnlCommandAttack.setResetComponent();
                this.pnlCommandAttack.setVisible(true);

                this.pnlCommandAttack.setMapPanelReference(this.mapPanel);

                this.mapPanel.getMapManager().setFlagConquest();
                this.player.setNArmiesForTurn();

                pnlCommand.revalidate();
                pnlCommand.repaint();
                break;

            case DEFENCE:
                this.lblTitle.setText("Utente : " + player.getPlayerID() + "  Difende");
                this.pnlInitReinforce.setVisible(false);

                BorderLayout layoutDefence = (BorderLayout)pnlCommand.getLayout();
                this.pnlCommand.remove(layoutDefence.getLayoutComponent(BorderLayout.CENTER));
                this.pnlCommand.add(pnlCommandDefence, BorderLayout.CENTER);

                this.pnlCommandDefence.setResetComponent();
                this.pnlCommandDefence.setVisible(true);

                this.pnlCommandDefence.setMapPanelReference(this.mapPanel);

                this.getPnlDefenceManager().getPnlStateReinforce().setPanelComponent_ON();
                this.getPnlDefenceManager().getPnlStateShift().setPanelComponent_OFF();

                pnlCommand.revalidate();
                pnlCommand.repaint();
                break;
        }
    }

    /**
     * Method to get the reference to MapPanel class
     *
     * @return  reference to mapPanel class
    */
    public MapPanel getMapPanel() {
        return mapPanel;
    }

    /**
     * Method to set the current reference to MapPanel class
     *
     * @param mp    the reference to mapPanel class
     */
    public void setMapPanelReference(MapPanel mp){this.mapPanel = mp;}

    /**
     * Method to get a reference to the class that manage the Init Reinforce turn
     *
     * @return  reference to InitReinforce class
     */
   public InitialReinforcePanel getPnlInitReinforce(){return pnlInitReinforce;}

    /**
     * Method to get a reference to the class that manage the component of Attack turn
     *
     * @return  reference to AttackManager class
     */
   public AttackManagerPanel getPnlAttackManager(){return this.pnlCommandAttack;}

    /**
     * Method to get a reference to the class that manage the component of Defence turn
     *
     * @return  reference to DefenceManager class
     */
   public DefenceManagerPanel getPnlDefenceManager(){return this.pnlCommandDefence;}

}