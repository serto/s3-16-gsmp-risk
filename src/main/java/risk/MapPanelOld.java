package risk;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Class to manage the design of map panel for the table of game as the ufficial Risiko game
 *
 * During the game the panel shows the world map with the territories of every player.
 * The map of table game is reprented by a image, set in Resource. The image is scaled according to the screen size,
 * to allow a correct display to the client.
 *
 * Every territory is represented by a button with a tank image of player color (red or blue) that reprents its owner,
 * and a number of armies that represents how many tanks there are on its region.
 * So, the player can always konw his territories and how many armies he has in everyone.
 *
 * According to the state of player turn, the territory buttons can be enabled or disabled, to allow player a better menage of theese:
 *  -  During the Initial turn of game: the system enables the territories the player owner of panel and disables the other player territories.
 *  -  During the Reinforce turn and Shift turn: the system enables the territories of attacking player and disables the defencing player territories.
 *  -  During the Attack turn: the system enables the defencing territories buttons when the player must select the defencing territory, and the attacking
 * territories buttons when the player must select the attacking territory.
 * In this way, the every player can play and select some button without risk to create mistakes.
 *
 * Than, according to the state game, selecting a territory button the system executes a different function on the map panel to allow an
 * upgrate of map according to the choices of players.
 *
 * @author Masi Elisabetta
 */

public class MapPanelOld extends JPanel {

    /* mapManager reference to class to manage the functions on the world map
    * pnlPlayer reference to class that manages the panel of player and his activities during his turn
    * player    reference to class that manages the player and his detail */
    private MapManagerOld mapManager;
    private PlayerPanel playerPanel;
    private Player player;

    //list of all territories in the world map
    private ArrayList<Territory> territories;

    //button array to manage every territory with a number of armies and a image of tank
    private JButton[] btnArms;

    //image of general world map and the red and blue tank to set the territory button
    private Image map, imgArmsRed, imgArmsBlu;


    /**
     * @param territories   list of all territories in the world map
     * @param playerPanel   reference to class that manages the panel of player and his activities during his turn
     * @param player        reference to class that manages the player and his detail
     */
/*    public MapPanelOld(ArrayList<Territory> territories, PlayerPanel playerPanel, Player player){
      this.setPreferredSize(new Dimension(1190, 790));
      this.setBackground(Color.DARK_GRAY);

      try{
          this.map = ImageIO.read(getClass().getResourceAsStream("/img/map.png"));
          map = getScaledImage(map,1190, 790);

          this.imgArmsBlu = ImageIO.read(getClass().getResourceAsStream("/img/armBlu.png")).getScaledInstance(38,35, 2);
          this.imgArmsRed = ImageIO.read(getClass().getResourceAsStream("/img/armRed.png")).getScaledInstance(35,35, 2);
      }catch(Exception e){
        throw new RuntimeException(e);
      }

      this.territories = territories;
      this.playerPanel = playerPanel;
      this.player = player;

      mapManager = new MapManagerOld(this, this.player, playerPanel);

      //create a button array to manage the territories as button on the map
      btnArms = new JButton[territories.size()];

      //create an interaction between the button and some panel
      createInteractionTerritoriesInMap();

      //to allow the correct set of button
      this.setLayout(null);
    }

    /**
     * Method override to paint the map image and all panel
     *
     * @param g     graphics with design
     */
 /*   @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        g.drawImage(map, 0, 0, null);
    }

    /**
     * Method to scale the image to allow a good resolution in every screen
     *
     * @param mapImg    image of world map to scale
     * @param width     width according to scale
     * @param height    height according to scale
     * @return          the new scaled image
     */
  /*  private Image getScaledImage(Image mapImg, int width, int height){
        BufferedImage resizedImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(mapImg, 0, 0, width, height, null);
        g2.dispose();

        return resizedImg;
    }


    /**
     * Method to create and menage the interaction between the player and game map.
     * For every territory in the list get by system:
     *      it get the coordination of territory section in the world map image
     *      in the array button create a new button
     *      set the bounds of button with the coordination get
     *      set the text of button with the number of territory armies (init = 1)
     *      set the button as disable so to exchange according to the state of game
     *      set the image of tank according to the player owner (red or blu)
     *
     * According to the state game, selecting a territory button the system executes a different function on the map panel
     */
  /*  private void createInteractionTerritoriesInMap(){
        for(int idTer = 0; idTer < territories.size(); idTer++) {
            //get the coordination of section of territory in the world map image
           int[] cord = mapManager.getPositionTerritory(idTer);

           //set the button of territory
           btnArms[idTer] = new JButton();
           btnArms[idTer].setBounds(cord[0], cord[1],90 , 50);
           btnArms[idTer].setText("" + territories.get(idTer).getArmies());
           btnArms[idTer].setEnabled(false);
           if (territories.get(idTer).getPlayerOwner().getPlayerID() == 0) {
               btnArms[idTer].setIcon(new ImageIcon(imgArmsRed));
           } else {
               btnArms[idTer].setIcon(new ImageIcon(imgArmsBlu));
           }
           this.add(btnArms[idTer]);

           //set the state game
           int idBtn = idTer;
           btnArms[idTer].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (mapManager.getState()){
                  case MapManager.INIT_REINFORCE:
                      int nArmies = mapManager.distributionArmies(idBtn);
                      ((JButton) e.getSource()).setText("" + nArmies);
                      mapManager.setNArmiesToINIT();
                      break;

                  case MapManager.REINFORCE:
                      int nNewArmies = mapManager.setNArmiesToReinforce(idBtn);
                      ((JButton) e.getSource()).setText("" + nNewArmies);
                      mapManager.setNArmies_ReinforceTurn();
                      break;

                  case MapManager.TERRITORY_DEFENCE:
                      mapManager.setTerritoryDefencing(idBtn);
                      break;

                  case MapManager.TERRITORY_ATTACKING:
                      mapManager.setTerritoryAttacking(idBtn);
                      break;

                  case MapManager.SHIFT_FROM:
                      mapManager.setShift_TerritoryFROM(idBtn);
                      break;

                  case MapManager.SHIFT_TO:
                      mapManager.setShift_TerritoryTO(idBtn);
                      break;
                }
               }
            });
        }
    }

    /**
     * Method to enable and disable all territory button in the map
     *
     * @param enabled   new state of territory button
     */
  /*  public void enabledAllButtons(boolean enabled){
        for(int idTer = 0; idTer < territories.size(); idTer++){
            btnArms[idTer].setEnabled(enabled);
        }
    }

    /**
     * Method to enable an disable the territory button of player owner of panel
     *
     * @param enabled new state of territory button
     */
  /*  public void enabledButtons(boolean enabled){
        for(int idTer = 0; idTer < territories.size(); idTer++) {
            if(territories.get(idTer).getPlayerOwner().getPlayerID() == this.player.getPlayerID()){
                btnArms[idTer].setEnabled(enabled);
            }
        }
    }


    /**
     * Method to return the mapManager reference class
     *
     * @return  reference of map manager class
     */
  /*  public MapManager getMapManager(){return this.mapManager;}


    /**
     * Method to get the list of territories to upgrade during the game
     *
     * @return  the list of territories

    public ArrayList<Territory> getTerritories(){ return this.territories; }

    /**
     * Method to set the list of territories with the upgraded data
     *
     * @param territories   the list of territories

    public void setTerritories(ArrayList<Territory> territories){ this.territories = territories;}

    /**
     * Method to set a new number armies to territory after the dice roll and attack
     *
     * @param pos       position of territory
     * @param armies    number of armies to add

    public void setArmiesToTerritory(int pos, int armies){ this.territories.get(pos).setNArmies(armies);}

    /**
     * Method to add new armies to territory in the relative position during the reinforce and shift
     *
     * @param pos       position of territory
     * @param armies    number of armies to add

    public void addArmiesToTerritory(int pos, int armies){ this.territories.get(pos).addArmies(armies);}

    /**
     * Method to remove some armies to territory in the relative position after the dice roll and attack and during the shift
     *
     * @param pos       position of territory
     * @param armies    number of armies to add

    public void removeArmiesToTerritory(int pos, int armies) { this.territories.get(pos).lessArmies(armies);}

    /**
     * Method to set the new owner for the relative territory and add the new territory to a relative list
     * of the player; after the attack turn
     *
     * @param pos       position of territory
     * @param player    player owner

    public void setPlayerOwnerTerritory(int pos, Player player){
        this.territories.get(pos).setPlayerOwner(player);
        player.addTerritory(pos);
    }

    /**
     * Method to update the territory detail after the attack and shift operations, when the territory detail
     * have been modify by the player choices.
     * The system use this method every time that the map has been updated. So according to the state of the game,
     * it enable the button of the player.
     *
     * @param territories   list of territories

    public void updateTerritories(ArrayList<Territory> territories){
        for(int idTer = 0; idTer < this.territories.size(); idTer++) {
            if(this.territories.get(idTer).getArmies() != territories.get(idTer).getArmies() ||
                    this.territories.get(idTer).getPlayerOwner() != territories.get(idTer).getPlayerOwner()){

                this.territories.get(idTer).setNArmies(territories.get(idTer).getArmies());
                this.territories.get(idTer).setPlayerOwner(territories.get(idTer).getPlayerOwner());

                btnArms[idTer].setText("" + this.territories.get(idTer).getArmies());

                if (this.territories.get(idTer).getPlayerOwner().getPlayerID() == 0) {
                    btnArms[idTer].setIcon(new ImageIcon(imgArmsRed));
                } else {
                    btnArms[idTer].setIcon(new ImageIcon(imgArmsBlu));
                }

                btnArms[idTer].revalidate();
                btnArms[idTer].repaint();
            }
        }

       //The game is in the state of INIT REINFORCE, so it enable all the button in the map
        if(mapManager.getState() == MapManager.INIT_REINFORCE){
           this.enabledButtons(true);
        }

        //The game ends the INIT REINFORCE, so it enable the next button to change the game state
        if(mapManager.getState() == MapManager.REINFORCE){
            playerPanel.getPnlInitReinforce().setBtnNext();
        }
    }

    /**
     * Method to set a new number of armies in every territory button on the map, after the attack or after the shift turn
     * If the attacking player won a new territory during the attack turn, the method set it with a new number of armies and
     * a new tank image of new player owner.
     *
     * @param idTerFrom     id from shift or attacking territory
     * @param idTerTo       id to shift or defencing territory

    public void updateNArmiesOfTerritory(int idTerFrom, int idTerTo){
        btnArms[idTerFrom].setText("" + territories.get(idTerFrom).getArmies());
        btnArms[idTerTo].setText("" + territories.get(idTerTo).getArmies());

        if (territories.get(idTerTo).getPlayerOwner().getPlayerID() == 0) {
            btnArms[idTerTo].setIcon(new ImageIcon(imgArmsRed));
        } else {
            btnArms[idTerTo].setIcon(new ImageIcon(imgArmsBlu));
        }
    }

    public void viewMessage(String message){
        JOptionPane.showMessageDialog(null, message);
    }*/
}

