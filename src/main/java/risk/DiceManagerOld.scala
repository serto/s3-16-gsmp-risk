package risk

import scala.util._
import scala.util.Sorting._

/**
  * Class to random extraction val to manage the attack and defence dices
  *
  * @author Masi Elisabetta
  */
class DiceManagerOld(){

  val rand = new Random
  var i = 0
  var elem = 0

  /**
    * Method to extraction the val for the dices
    * It return a int array of random val between 0 and 6, as well as the faces of a dice.
    * The length of array represent the number of dices of the player and it is sort in increasing way
    * to allow a verify easy
    * */
  def extractionValDices(nDices: Int): Array[Int] ={

    val valDices = new Array[Int](nDices + 1)

    for (i <- 0 to nDices) {
      elem = 1 + rand.nextInt(6)
      valDices(i) = elem
    }

    quickSort[Int](valDices)(Ordering[Int].reverse)

    valDices
  }
}