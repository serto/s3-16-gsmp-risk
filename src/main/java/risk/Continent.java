package risk;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Class which contains the lists with all territory ids of a continent
 *
 * @author Genghini Luca
 */

public class Continent {

    //Continenti
    public final static List<Integer> NORTH_AterritoryList = Collections.unmodifiableList(Arrays.asList(0,1,2,3,4,5,6,7,8));
    public final static List<Integer> SOUTH_AterritoryList = Collections.unmodifiableList(Arrays.asList(9,10,11,12));
    public final static List<Integer> EUROPEterritoryList = Collections.unmodifiableList(Arrays.asList(13,14,15,16,17,18,19));
    public final static List<Integer> AFRICAterritoryList = Collections.unmodifiableList(Arrays.asList(20,21,22,23,24,25));
    public final static List<Integer> ASIAterritoryList = Collections.unmodifiableList(Arrays.asList(26,27,28,29,30,31,32,33,34,35,36,37));
    public final static List<Integer> AUSTRALIAterritoryList = Collections.unmodifiableList(Arrays.asList(38,39,40,41));

   }
