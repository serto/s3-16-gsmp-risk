package risk.defence;

import risk.StateGamePanel;

import javax.swing.*;
import java.awt.*;

/**
 * Class to manage the Defence turn during Defence round state.
 *
 * In particular, it show the territories selected by the attacking player as attacking and defencing territories and how many
 * armies he choose to attack.
 * Whenever the attacking player selects a button, the server sends a new message to this defencing player to update him with new data.
 * Thanks this gui the defencing player can play with other player and follow the game and the attacking player choices in streaming,
 * know the result of dices and how many armies the territories lose.
 *
 * @author Masi Elisabetta
 */
public class DefenceStatePanel extends JPanel implements StateGamePanel{

    //reference to class that manages the panel of player and his activities during his turn
    private DefenceManagerPanel pnlDefenceManager;

    //components of panel to show the activities of player
    private JPanel pnlAttack, pnlDetail, pnlAttacking, pnlDefencing, pnlNArmies, pnlResult, pnlDescr, pnlResultDices;
    private JLabel lblFrom, lblTo, lblNArmies, lblDetailResult, lblResultA, lblResultD;

    public DefenceStatePanel(DefenceManagerPanel defenceManagerPanel){

        this.pnlDefenceManager = defenceManagerPanel;

        this.setLayout(new BorderLayout());
        this.setBackground(Color.LIGHT_GRAY);

        this.pnlAttack = new JPanel(new BorderLayout());

        JLabel lblTitle = new JLabel("2 - ATTACCO");
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 14));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlAttack.add(lblTitle, BorderLayout.NORTH);

        this.pnlDetail = new JPanel(new BorderLayout());
        this.pnlAttacking = new JPanel(new BorderLayout());
        JLabel lblAttack = new JLabel("DA: ");
        lblAttack.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlAttacking.add(lblAttack, BorderLayout.NORTH);
        this.lblFrom = new JLabel(" ");
        this.lblFrom.setFont(new Font("Arial", Font.BOLD, 16));
        this.lblFrom.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlAttacking.add(lblFrom, BorderLayout.CENTER);
        this.pnlDetail.add(pnlAttacking, BorderLayout.NORTH);

        this.pnlDefencing = new JPanel(new BorderLayout());
        JLabel lblDefence = new JLabel("VERSO: ");
        lblDefence.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDefencing.add(lblDefence, BorderLayout.CENTER);
        this.lblTo = new JLabel(" ");
        this.lblTo.setFont(new Font("Arial", Font.BOLD, 16));
        this.lblTo.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDefencing.add(lblTo, BorderLayout.SOUTH);
        this.pnlDetail.add(pnlDefencing, BorderLayout.CENTER);
        this.pnlAttack.add(pnlDetail, BorderLayout.CENTER);

        this.pnlNArmies = new JPanel(new BorderLayout());
        JLabel lblDetailArmies = new JLabel("Con : ");
        lblDetailArmies.setHorizontalAlignment(SwingConstants.LEFT);
        this.pnlNArmies.add(lblDetailArmies, BorderLayout.WEST);
        this.lblNArmies = new JLabel("");
        this.lblNArmies.setFont(new Font("Arial", Font.BOLD, 14));
        this.lblNArmies.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlNArmies.add(lblNArmies, BorderLayout.CENTER);
        JLabel lblDetailArmies1 = new JLabel("armate");
        lblDetailArmies1.setHorizontalAlignment(SwingConstants.RIGHT);
        this.pnlNArmies.add(lblDetailArmies1, BorderLayout.EAST);
        this.pnlAttack.add(pnlNArmies, BorderLayout.SOUTH);
        this.add(pnlAttack, BorderLayout.NORTH);

        this.pnlResult = new JPanel(new BorderLayout());
        this.pnlDescr = new JPanel(new BorderLayout());
        this.pnlDescr.setPreferredSize(new Dimension(120, 60));
        JSeparator separator = new JSeparator();
        this.pnlDescr.add(separator, BorderLayout.NORTH);

        JLabel lblDetail = new JLabel("Dal risultato dei dadi:");
        lblDetail.setFont(new Font("Arial", Font.ITALIC, 16));
        lblDetail.setHorizontalAlignment(SwingConstants.CENTER);
        lblDetail.setForeground(Color.BLACK);
        this.pnlDescr.add(lblDetail, BorderLayout.CENTER);

        this.lblDetailResult = new JLabel("\nVINCE...");
        this.lblDetailResult.setFont(new Font("Arial", Font.ITALIC, 12));
        this.lblDetailResult.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDescr.add(lblDetailResult, BorderLayout.SOUTH);
        this.pnlResult.add(pnlDescr, BorderLayout.NORTH);

        this.pnlResultDices = new JPanel(new BorderLayout());
        this.lblResultA = new JLabel(" L'attacco perde: ");
        this.lblResultA.setFont(new Font("Arial",  Font.ITALIC, 14));
        this.lblResultA.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlResultDices.add(lblResultA, BorderLayout.NORTH);

        this.lblResultD = new JLabel(" La difesa perde:  ");
        this.lblResultD.setFont(new Font("Arial", Font.ITALIC, 14));
        this.lblResultD.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlResultDices.add(lblResultD, BorderLayout.CENTER);
        this.pnlResult.add(pnlResultDices, BorderLayout.CENTER);
        this.add(pnlResult, BorderLayout.CENTER);
    }

    /**
     * Method to set the relative label in defencing player panel with the attack data selected by attacking player in his panel
     *
     * @param idTerA    id territory chosen as attacking territory
     * @param idTerD    id territory chosen as defencing territory
     * @param nArmies   number with which the attacking player has chosen to attack
     */
    public void setLblTerritories(int idTerA, int idTerD, int nArmies){
        this.lblFrom.setText(" " + pnlDefenceManager.getMapPanel().getTerritories().get(idTerA).getName());
        this.lblTo.setText(" " + pnlDefenceManager.getMapPanel().getTerritories().get(idTerD).getName());
        this.lblNArmies.setText(" " + nArmies);
    }

    /**
     * Method to set the labels in result panel after the dices roll with its result
     *
     * @param winner    name of the winner
     * @param armiesA   number of armies lost by attack
     * @param armiesD   number of armies lost by defence
     */
    public void setResult(String winner, int armiesA, int armiesD){
        this.lblDetailResult.setText("\nVINCE ...\n" + winner + " !\n");
        this.lblDetailResult.setFont(new Font("Arial", Font.BOLD, 18));
        this.lblDetailResult.setForeground(Color.BLUE);
        this.lblResultA.setText(" L'attacco perde: " + armiesA);
        this.lblResultA.setFont(new Font("Arial", Font.ITALIC, 14));
        this.lblResultD.setText(" La difesa perde:  " + armiesD);
        this.lblResultD.setFont(new Font("Arial", Font.ITALIC, 14));
    }

    /**
     * Method to enable the defence panel when the attacking player ends the reinforce turn
     */
    @Override
    public void setPanelComponent_ON() {
        this.setBackground(Color.CYAN);
        this.pnlAttack.setBackground(Color.CYAN);
        this.pnlAttacking.setBackground(Color.CYAN);
        this.pnlDefencing.setBackground(Color.CYAN);
        this.pnlDetail.setBackground(Color.CYAN);
        this.pnlNArmies.setBackground(Color.CYAN);
    }

    /**
     * Method to reset the component of panel when start a new turn
     */
    @Override
    public void resetComponent() {
        this.lblFrom.setText(" ");
        this.lblTo.setText(" ");
        this.lblNArmies.setText(" ");

        this.lblDetailResult.setText("\nVINCE...");
        this.lblDetailResult.setFont(new Font("Arial", Font.ITALIC, 12));
        this.lblDetailResult.setForeground(Color.BLACK);
        this.lblResultA.setText(" L'attacco perde: ");
        this.lblResultA.setFont(new Font("Arial",  Font.ITALIC, 14));
        this.lblResultD.setText(" La difesa perde:  ");
        this.lblResultD.setFont(new Font("Arial", Font.ITALIC, 14));
    }

    /**
     * Method to enable all defence panel when the attacking player is rolling dices
     */
    public void setPanelResult_ON(){
        this.setPanelComponent_ON();
        this.pnlResult.setBackground(Color.CYAN);
        this.pnlDescr.setBackground(Color.CYAN);
        this.pnlResultDices.setBackground(Color.CYAN);
    }


    /**
     * Method to disable the panel when the attacking player doesn't choose to attack or ends his turn
     */
    @Override
    public void setPanelComponent_OFF() {
        this.setBackground(Color.LIGHT_GRAY);
        this.pnlAttack.setBackground(Color.LIGHT_GRAY);
        this.pnlAttacking.setBackground(Color.LIGHT_GRAY);
        this.pnlDefencing.setBackground(Color.LIGHT_GRAY);
        this.pnlDetail.setBackground(Color.LIGHT_GRAY);
        this.pnlNArmies.setBackground(Color.LIGHT_GRAY);

        this.pnlResult.setBackground(Color.LIGHT_GRAY);
        this.pnlDescr.setBackground(Color.LIGHT_GRAY);
        this.pnlResultDices.setBackground(Color.LIGHT_GRAY);
    }

    /**
     * Method to disable the Result panel and reset the labels when, after dice roll, the attacking player chooses to attack yet
     */
    public void setPanelOFF_AfterDices(){
        this.pnlResult.setBackground(Color.LIGHT_GRAY);
        this.pnlDescr.setBackground(Color.LIGHT_GRAY);
        this.pnlResultDices.setBackground(Color.LIGHT_GRAY);

        this.lblFrom.setText(" ");
        this.lblTo.setText(" ");
        this.lblNArmies.setText(" ");
        this.setResult(" ", 0 , 0 );
    }
}
