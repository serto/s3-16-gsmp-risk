package risk.defence;

import risk.MapPanel;

import javax.swing.*;
import java.awt.*;


/**
 *  Class to create and manage the component of the different panel in the defence turn.
 *
 *  Each panel manages a different section during defence turn and it is connect with the relative panel
 *  in the attack turn. Each defence panel has the scope to report the attacking player choices to allow
 *  the defencing player to know the detail and play his turn in real time way with other player.
 *
 * @autor Masi Elisabetta
 */
public class DefenceManagerPanel extends JPanel {

    // New panel that compose the different section of defence turn
    private ReinforceStatePanel pnlStateReinforce;
    private DefenceStatePanel pnlStateDefence;
    private ShiftStatePanel pnlStateShift;

    // mapPanel  reference to class that manages the game world map table
    private MapPanel mapPanel;

    public DefenceManagerPanel(){

        this.setLayout(new BorderLayout());

        pnlStateReinforce = new ReinforceStatePanel();
        this.add(pnlStateReinforce, BorderLayout.NORTH);

        pnlStateDefence = new DefenceStatePanel(this);
        this.add(pnlStateDefence, BorderLayout.CENTER);

        pnlStateShift = new ShiftStatePanel(this);
        this.add(pnlStateShift, BorderLayout.SOUTH);

        this.setVisible(false);
    }

    /**
     * Method to reset some component of the panel when the system set the player panel as turn after the first
     */
    public void setResetComponent(){
        this.pnlStateDefence.resetComponent();
        this.pnlStateShift.resetComponent();
    }

    /**
     * Method to enable the defence section when the other player is in the attack turn
     */
    public void setPnlStateDefence(){
        this.pnlStateReinforce.setPanelComponent_OFF();
        this.pnlStateDefence.setPanelComponent_ON();
    }

    /**
     * Method to enable the shift section when the other player is in the shift turn
     */
    public void setPnlStateShift(){
        this.pnlStateDefence.setPanelComponent_OFF();
        this.pnlStateShift.setPanelComponent_ON();
    }

    /**
     * Method to enable the dice result section when the other player has roll dices
     */
    public void setPnlAfterDices(){
        this.pnlStateDefence.setPanelOFF_AfterDices();
    }

    /**
     * Method to get the reference to MapPanel class
     *
     * @return  reference to mapPanel class
     */
    public MapPanel getMapPanel() {
        return mapPanel;
    }

    /**
     * Method to set the current reference to MapPanel class
     *
     * @param mp    the reference to mapPanel class
     */
    public void setMapPanelReference(MapPanel mp){this.mapPanel = mp;}

    /**
     * Method to get the reference to defence section that show the reinforce turn
     *
     * @return reference to the reinforce section
     */
    public ReinforceStatePanel getPnlStateReinforce(){return this.pnlStateReinforce;}

    /**
     * Method to get the reference to defence section that show the attack choice of other player
     *
     * @return  reinforce to the defence section
     */
    public DefenceStatePanel getPnlStateDefence(){ return this.pnlStateDefence;}

    /**
     * Method to get the reference to defence section that show the shift choice of other player
     *
     * @return  reference to the shift section
     */
    public ShiftStatePanel getPnlStateShift(){ return this.pnlStateShift;}
}
