package risk.defence;

import risk.StateGamePanel;

import javax.swing.*;
import java.awt.*;

/**
 * Class to manage the show of data shift selected by attacking player.
 *
 * Whenever the attacking player selects a button, the server sends a new message to this defencing player to update him with new data.
 * Thanks this gui, the defencing player can play with other player and follow the game and the attacking player choices in streaming.
 * In particular he can know the territory selected by attacking player as territories from/to which to shift.
 *
 * @author Masi Elisabetta
 */
public class ShiftStatePanel extends JPanel implements StateGamePanel{

    //reference to class that manages the panel of player and his activities during his turn
    private DefenceManagerPanel pnlDefenceManager;

    //components of panel to show the activities of player
    private final JPanel pnlTitle, pnlDetail, pnlDetailFrom, pnlDetailTo;
    private final JLabel lblFrom, lblTo;

    public ShiftStatePanel(DefenceManagerPanel defenceManagerPanel) {

        this.pnlDefenceManager = defenceManagerPanel;

        this.setLayout(new BorderLayout());
        this.setBackground(Color.LIGHT_GRAY);
        this.setPreferredSize(new Dimension(120, 70));

        this.pnlTitle = new JPanel(new BorderLayout());
        JSeparator separator = new JSeparator();
        this.pnlTitle.add(separator, BorderLayout.NORTH);
        JLabel lblTitle = new JLabel("3 - SPOSTAMENTO");
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 14));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlTitle.add(lblTitle, BorderLayout.CENTER);
        this.add(pnlTitle, BorderLayout.NORTH);

        this.pnlDetail = new JPanel(new BorderLayout());
        this.pnlDetailFrom = new JPanel(new BorderLayout());
        JLabel lblDetailFrom = new JLabel("DA");
        lblDetailFrom.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDetailFrom.add(lblDetailFrom, BorderLayout.WEST);
        this.lblFrom = new JLabel(" ");
        this.pnlDetailFrom.add(lblFrom, BorderLayout.CENTER);
        this.pnlDetail.add(pnlDetailFrom, BorderLayout.NORTH);

        this.pnlDetailTo = new JPanel(new BorderLayout());
        JLabel lblDetailTo = new JLabel("A");
        lblDetailTo.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDetailTo.add(lblDetailTo, BorderLayout.WEST);
        this.lblTo = new JLabel(" ");
        this.pnlDetailTo.add(lblTo, BorderLayout.CENTER);
        this.pnlDetail.add(pnlDetailTo, BorderLayout.CENTER);
        this.add(pnlDetail, BorderLayout.CENTER);
    }

    /**
     * Method to set the labels in panel with the territories name selected by attacking panel in his shift turn
     *
     * @param idTerFrom     id territory selected as from shift territory
     * @param idTerTo       id territory selected as to shift territory
     */
    public void setLblTerritories(int idTerFrom, int idTerTo){
        this.lblFrom.setText(" " + this.pnlDefenceManager.getMapPanel().getTerritories().get(idTerFrom).getName());
        this.lblTo.setText(" " + this.pnlDefenceManager.getMapPanel().getTerritories().get(idTerTo).getName());
    }

    /**
     * Method to enable the panel when the attacking player is shifting his armies
     */
    @Override
    public void setPanelComponent_ON() {
        this.setBackground(Color.CYAN);
        this.pnlTitle.setBackground(Color.CYAN);
        this.pnlDetail.setBackground(Color.CYAN);
        this.pnlDetailFrom.setBackground(Color.CYAN);
        this.pnlDetailTo.setBackground(Color.CYAN);
    }

    /**
     * Method to reset the component of panel when start a new turn
     */
    @Override
    public void resetComponent() {
        this.lblFrom.setText(" ");
        this.lblTo.setText(" ");
    }

    /**
     * Method to disable the panel when the attacking player ends his turn of attack
     */
    @Override
    public void setPanelComponent_OFF() {
        this.setBackground(Color.LIGHT_GRAY);
        this.pnlTitle.setBackground(Color.LIGHT_GRAY);
        this.pnlDetail.setBackground(Color.LIGHT_GRAY);
        this.pnlDetailFrom.setBackground(Color.LIGHT_GRAY);
        this.pnlDetailTo.setBackground(Color.LIGHT_GRAY);
    }
}
