package risk.defence;

import risk.StateGamePanel;

import javax.swing.*;
import java.awt.*;

/**
 * Class to manage the Defence round.
 * It contains two panel: a general panel with title and same info and a panel to show to player when attacking player is renforcing his territories.
 * Whenever the attacking player selects a button, the server sends a new message to this defencing player to update him with new data.
 * Thanks this gui the defencing player can play with other player and follow the game and the attacking player choices during the renforce turn
 * in streaming.
 *
 * @author Masi Elisabetta
 */
public class ReinforceStatePanel extends JPanel implements StateGamePanel{

    //components of panel to show the activities of player
    private JPanel pnlGame, pnlReinforce;

    public ReinforceStatePanel(){

        this.setLayout(new BorderLayout());
        this.setBackground(Color.LIGHT_GRAY);

        JLabel lblTitle = new JLabel("DIFESA");
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setBackground(Color.DARK_GRAY);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 18));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lblTitle, BorderLayout.NORTH);

        JPanel pnlDetail = new JPanel(new BorderLayout());
        JLabel lblDetail = new JLabel("Il tuo avversario sta giocando!");
        lblDetail.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel lblDetail1 = new JLabel("E' in fase di: ");
        lblDetail1.setHorizontalAlignment(SwingConstants.CENTER);
        pnlDetail.add(lblDetail, BorderLayout.CENTER);
        pnlDetail.add(lblDetail1, BorderLayout.SOUTH);
        this.add(pnlDetail, BorderLayout.CENTER);

        this.pnlGame = new JPanel(new BorderLayout());
        JSeparator separator = new JSeparator();
        this.pnlGame.add(separator, BorderLayout.NORTH);
        this.pnlReinforce = new JPanel(new BorderLayout());
        this.pnlReinforce.setPreferredSize(new Dimension(120, 40));
        JLabel lblReinforce = new JLabel("1 - RINFORZO");
        lblReinforce.setFont(new Font("Arial", Font.BOLD, 14));
        lblReinforce.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlReinforce.add(lblReinforce, BorderLayout.CENTER);
        this.pnlGame.add(pnlReinforce, BorderLayout.CENTER);
        separator = new JSeparator();
        this.pnlGame.add(separator, BorderLayout.SOUTH);
        this.add(pnlGame, BorderLayout.SOUTH);
    }

    /**
     * Method to enable the reinforce panel when the attacking player is reinforcing his territories
     */
    @Override
    public void setPanelComponent_ON() {
        this.pnlReinforce.setBackground(Color.CYAN);
    }

    /**
     * Method to reset the component of panel when start a new turn
     */
    @Override
    public void resetComponent() {

    }

    /**
     * Method to disable the panel when the attacking player end to reinforce his territories
     */
    @Override
    public void setPanelComponent_OFF() {
        this.setBackground(Color.LIGHT_GRAY);
        this.pnlGame.setBackground(Color.LIGHT_GRAY);
        this.pnlReinforce.setBackground(Color.LIGHT_GRAY);
    }
}
