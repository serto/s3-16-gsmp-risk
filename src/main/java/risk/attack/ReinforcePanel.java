package risk.attack;

import risk.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Class to manage the Reinforce turn as initial section of Attack round.
 * Whenever that the player is in the attack round, as the first time he must distribute same armies among his territories.
 * The number of theese is update initially according to number of his territories, continents and same bonus.
 * Then, during this round, every time that the player selects a territory on the game map, the number of player armies is
 * decremented and the relative label in this panel is updated.
 *
 * When the player ends his armies to distribute, the player can select the button "next" (that has been enabled) and go to
 * the attack state. Meanwhile the client send a new message to server to update the panel and game map of defence player.
 *
 * @author Masi Elisabetta
 */
public class ReinforcePanel extends JPanel implements StateGamePanel {

    /* pnlAttackManager reference to class that manages the panel of player and his activities during his turn
       player    reference to class that manages the player and his detail */
    private Player player;
    private AttackManagerPanel pnlAttackManager;

    // Components of panel to allow the activities of player
    private JPanel pnlDetail, pnlDetailChoose;
    private JLabel lblNumCurrArmies;
    private JButton btnNext;

    /**
     * @param attackManagerPanel reference to panel of the current player
     * @param playerCurr  reference to player and his detail
     */
    public ReinforcePanel(AttackManagerPanel attackManagerPanel, Player playerCurr){

        this.pnlAttackManager = attackManagerPanel;
        this.player = playerCurr;

        this.setLayout(new BorderLayout());

        JLabel lblTitle = new JLabel("1 - RINFORZA");
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 14));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lblTitle, BorderLayout.NORTH);

        this.pnlDetail = new JPanel(new BorderLayout());
        JLabel lblNArmiesDetail = new JLabel("Numero armate da distribuire");
        lblNArmiesDetail.setHorizontalAlignment(SwingConstants.CENTER);
        lblNArmiesDetail.setFont(new Font("Arial",  Font.PLAIN, 13));
        this.pnlDetail.add(lblNArmiesDetail, BorderLayout.NORTH);

        this.lblNumCurrArmies = new JLabel(" " + player.getNArmies());
        this.lblNumCurrArmies.setHorizontalAlignment(SwingConstants.CENTER);
        this.lblNumCurrArmies.setFont(new Font("Arial",  Font.BOLD, 14));
        this.pnlDetail.add(lblNumCurrArmies, BorderLayout.CENTER);

        this.pnlDetailChoose = new JPanel(new BorderLayout());
        JLabel lblChoose1 = new JLabel("clicca sul territorio dove ");
        lblChoose1.setHorizontalAlignment(SwingConstants.CENTER);
        lblChoose1.setFont(new Font("Arial",  Font.PLAIN, 13));
        JLabel lblChoose2 = new JLabel("vuoi posizionare le tue armate");
        lblChoose2.setHorizontalAlignment(SwingConstants.CENTER);
        lblChoose2.setFont(new Font("Arial",  Font.PLAIN, 13));
        this.pnlDetailChoose.add(lblChoose1, BorderLayout.CENTER);
        this.pnlDetailChoose.add(lblChoose2, BorderLayout.SOUTH);
        this.pnlDetail.add(pnlDetailChoose, BorderLayout.SOUTH);
        this.add(pnlDetail, BorderLayout.CENTER);

        this.btnNext = new JButton("Next");
        this.btnNext.setEnabled(false);
        this.btnNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlAttackManager.setPnlAttack1();
                btnNext.setEnabled(false);
                player.sendMessage("Press_next");
            }
        });
        this.add(btnNext, BorderLayout.SOUTH);
    }

    /**
     * Method to update the label of number armies of player during the reinforce turn.
     * In this way, the player can always know the updated number of the armies that he can distribute yet.
     *
     * @param newNArmies  updated armies number of player
     */
    public void setLblNArmiesInThisTurn(int newNArmies){ this.lblNumCurrArmies.setText(""+newNArmies);}

    /**
     * Method to enable the button "next" to allow the player to go to the new attack state of game and client
     * to send a message to server and update the panel and map of defence player.
     */
    public void setBtnNext(){this.btnNext.setEnabled(true);}

    /**
     * Method to enable the panel when the player is in attack round so player panel is in attack state
     */
    @Override
    public void setPanelComponent_ON() {
        this.setBackground(Color.RED);
        this.pnlDetail.setBackground(Color.RED);
        this.pnlDetailChoose.setBackground(Color.RED);
        this.lblNumCurrArmies.setText("" + this.player.getNArmies());
    }

    /**
     * Method to reset some component of the panel when the system set the player panel as turn after the first
     */
    @Override
    public void resetComponent() {

    }

    /**
     * Method to disable the panel when the player ends his amies to distribute so his the renforce
     */
    @Override
    public void setPanelComponent_OFF() {
        this.setBackground(Color.LIGHT_GRAY);
        this.pnlDetail.setBackground(Color.LIGHT_GRAY);
        this.pnlDetailChoose.setBackground(Color.LIGHT_GRAY);
    }
}