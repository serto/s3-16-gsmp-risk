package risk.attack;

import esScala.DicesManager;
import esScala.State;
import message.SendAttackCarried;
import risk.Player;
import risk.StateGamePanel;

import javax.swing.*;
import java.awt.*;

/**
 * Class to manage the Attack turn.
 *
 * The player has choosen to attack the defence player and select the territory to and from attack with relative button in the panel.
 * He can choose the number of armies with which attack: according to the armies of attacking territory, with the relative button "minus" or "plus"
 * the player can add or less the number of armies.
 * When he sets up the attack, he selects the button "next" and he can start the dice roll.
 * In this case the client sends a new message to server to comunicate the player chose and to update the defence panel.
 *
 * @author Masi Elisabetta
 */

public class AttackPanel2 extends JPanel implements StateGamePanel {

    /* mapPanel  reference to class that manages the game map table
       pnlAttackManager reference to class that manages the panel of player and his activities during his turn
       player    reference to class that manages the player and his detail */
    private AttackManagerPanel pnlAttackManager;
    private Player player;

    // Components of panel to allow the activities of player
    private JPanel pnlDetail, pnlDetailFrom, pnlDetailTo;
    private JLabel lblFrom, lblTo, lblNArmies;
    private JButton btnAttack, btnDefence, btnNext, btnPlus, btnMinus;

    /*  nArmiesOfTerritory  number armies of territory selected as attacking
        nArmiesToAttacking  number armies with which the player chooses to attack
        nArmiesToDefence    number armies of territory that the player chooses to attack, so selected as defencing */
    private int nArmiesOfTerritory, nArmiesToAttacking, nArmiesToDefence;

    /**
     * @param attackManagerPanel reference to panel of the current player
     * @param playerCurr  reference to player and his detail
     */
    public AttackPanel2(AttackManagerPanel attackManagerPanel, Player playerCurr){
        this.pnlAttackManager = attackManagerPanel;
        this.player = playerCurr;

        this.setLayout(new BorderLayout());
        this.setBackground(Color.LIGHT_GRAY);

        JLabel lblTitle = new JLabel("3 - ATTACCO");
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 14));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lblTitle, BorderLayout.NORTH);

        JPanel pnlAttack = new JPanel(new BorderLayout());
        this.pnlDetail = new JPanel(new BorderLayout());
        this.pnlDetailTo = new JPanel(new BorderLayout());
        this.btnDefence = new JButton("VERSO");
        this.btnDefence.setEnabled(false);
        this.btnDefence.addActionListener(
                e -> pnlAttackManager.getMapPanel().getMapManager().state().setState_(State.TERRITORY_DEFENCE()));
        this.btnDefence.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDetailTo.add(btnDefence, BorderLayout.CENTER);
        this.lblTo = new JLabel(" ");
        this.lblTo.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDetailTo.add(lblTo, BorderLayout.SOUTH);
        this.pnlDetail.add(pnlDetailTo, BorderLayout.NORTH);

        this.pnlDetailFrom = new JPanel(new BorderLayout());
        this.btnAttack = new JButton("DA");
        this.btnAttack.setEnabled(false);
        this.btnAttack.addActionListener(e -> {
            pnlAttackManager.getMapPanel().getMapManager().state().setState_(State.TERRITORY_ATTACKING());
            nArmiesToDefence = pnlAttackManager.getMapPanel().getMapManager().getArmiesTerritoryDefence();
            btnDefence.setEnabled(false);
            btnNext.setEnabled(true);
        });
        this.btnAttack.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDetailFrom.add(btnAttack, BorderLayout.NORTH);
        this.lblFrom = new JLabel(" ");
        this.lblFrom.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDetailFrom.add(lblFrom, BorderLayout.CENTER);
        this.pnlDetail.add(pnlDetailFrom, BorderLayout.CENTER);
        pnlAttack.add(pnlDetail, BorderLayout.CENTER);

        JPanel pnlNArmies = new JPanel(new BorderLayout());
        JLabel lblDetailArm = new JLabel("con quante armate?");
        lblDetailArm.setHorizontalAlignment(SwingConstants.CENTER);
        pnlNArmies.add(lblDetailArm, BorderLayout.NORTH);

        this.btnMinus = new JButton("-");
        this.btnMinus.setEnabled(false);
        this.btnMinus.setPreferredSize(new Dimension(50,50));
        this.btnMinus.addActionListener(e -> {
            if(!btnPlus.isEnabled()){
                btnPlus.setEnabled(true); }
            if(nArmiesToAttacking > 0){
                nArmiesToAttacking--;
                lblNArmies.setText(String.valueOf(nArmiesToAttacking--));
            }else{
                btnMinus.setEnabled(false);
            }
        });
        pnlNArmies.add(this.btnMinus, BorderLayout.WEST);

        this.lblNArmies = new JLabel("");
        this.lblNArmies.setHorizontalAlignment(SwingConstants.CENTER);
        pnlNArmies.add(lblNArmies);

        this.btnPlus = new JButton("+");
        this.btnPlus.setEnabled(false);
        this.btnPlus.setPreferredSize(new Dimension(50,50));
        this.btnPlus.addActionListener(e -> {
            if(!btnMinus.isEnabled()){
                btnMinus.setEnabled(true);}
            if(nArmiesOfTerritory > nArmiesToAttacking){
                nArmiesToAttacking++;
                lblNArmies.setText(String.valueOf(nArmiesToAttacking));
            }else{
                btnPlus.setEnabled(false);
            }
        });
        pnlNArmies.add(this.btnPlus, BorderLayout.EAST);
        pnlAttack.add(pnlNArmies, BorderLayout.SOUTH);
        this.add(pnlAttack, BorderLayout.CENTER);

        this.btnNext = new JButton("NEXT");
        btnNext.addActionListener(e -> {
            player.sendMessage(new SendAttackCarried(pnlAttackManager.getMapPanel().getMapManager().territoryAttack().idTerritory(), pnlAttackManager.getMapPanel().getMapManager().territoryDefence().idTerritory(), nArmiesToAttacking));
            pnlAttackManager.getMapPanel().getMapManager().state().setState_(State.DICES());
            btnAttack.setEnabled(false);
            btnPlus.setEnabled(false);
            btnMinus.setEnabled(false);
            //new DicePanelOld(pnlAttackManager, nArmiesToAttacking, nArmiesToDefence, player);

            new DicesManager.DicesManager_Refactored(pnlAttackManager, nArmiesToAttacking, nArmiesToDefence, player);
        });
        this.btnNext.setEnabled(false);
        this.add(btnNext, BorderLayout.SOUTH);
    }

    /**
     * Method to set the label with the name of territory that player selects on the map as defencing territory
     * and to enable the buttons to allow player to select the attacking territory and save the defencing territory name
     *
     * @param nameDefencingTerritory   name of territory that player chooses to attack
     */
    public void setLblTo(String nameDefencingTerritory){
        this.lblTo.setText(nameDefencingTerritory);
        this.pnlDetailTo.setEnabled(false);
        this.pnlDetailFrom.setEnabled(true);
    }

    /**
     * Method to enable the new section of panel to allow the selection of attacking territory
     */
    public void enableAttackSection(){
        this.btnDefence.setEnabled(false);
        this.pnlDetailFrom.setBackground(Color.RED);
        this.btnAttack.setEnabled(true);
    }

    /**
     * Method to set the label with the name of territory that player selects on the map as attacking territory
     *
     * @param nameAttackingTerritory name of territory with which the player chooses to attack
     */
    public void setLblFrom(String nameAttackingTerritory){
        this.lblFrom.setText(nameAttackingTerritory);
    }

    /**
     * Method to set the label that show the number of armies of attacking territory.
     * This allow player to choose with how many armies attacking the other territory of defence.
     * Setting the variables the class manages the add or less of number armies allowing player a better chose.
     *
     * @param nArmies  number of armies of attacking territory
     */
    public void setLblNArmies (int nArmies){
        this.nArmiesToAttacking = nArmies - 1;
        this.nArmiesOfTerritory = nArmies - 1;
        this.lblNArmies.setText("" + nArmiesToAttacking);
    }

    /**
     * Method to enable and disable the minus and plus buttons according to the state of game.
     * When the player choose the territories to attack and defence the buttons are enabled, otherwise they are disable.
     *
     * @param newStateButton   new state for the button of panel
     */
    public void enabledBtnAttackPnl(boolean newStateButton){
        this.btnMinus.setEnabled(newStateButton);
        this.btnPlus.setEnabled(newStateButton);
    }


    /**
     * Method to enable the panel when the player choose to attack
     */
    @Override
    public void setPanelComponent_ON(){
        this.setBackground(Color.RED);
        this.pnlDetail.setBackground(Color.RED);
        this.pnlDetailTo.setBackground(Color.RED);
        this.pnlDetailFrom.setBackground(Color.LIGHT_GRAY);
        this.btnDefence.setEnabled(true);
        this.btnNext.setEnabled(false);
    }


    /**
     * Method to reset some component of the panel when the system set the player panel as turn after the first
     */
    @Override
    public void resetComponent() {
        this.lblTo.setText(" ");
        this.lblFrom.setText(" ");
        this.lblNArmies.setText(" ");
    }

    /**
     * Method to disable the panel when the player choose not attack so end his round, or when he ends his attack
     */
    @Override
    public void setPanelComponent_OFF(){
        this.setBackground(Color.LIGHT_GRAY);
        this.pnlDetail.setBackground(Color.LIGHT_GRAY);
        this.pnlDetailFrom.setBackground(Color.LIGHT_GRAY);
        this.pnlDetailTo.setBackground(Color.LIGHT_GRAY);
        this.btnNext.setEnabled(false);
    }
}