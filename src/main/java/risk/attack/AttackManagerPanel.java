package risk.attack;

import risk.MapPanel;
import risk.Player;
import risk.PlayerPanel;

import javax.swing.*;
import java.awt.*;


/**
 * Class to create and manage the component of the different panel in the attack turn.
 *
 * Each panel manages a different section during attack turn: Reinforce, choice if attack, Attack and Shift;
 * as well as the official Risiko game.
 * During the game, the system enables and disables the panel according to need, in this way the player
 * can know which activity game he can do.
 *
 * @author Masi Elisabetta
 */
public class AttackManagerPanel extends JPanel {

    // New panel that compose the different section of attack turn
    private ReinforcePanel pnlReinforce;
    private AttackPanel1 pnlAttack1;
    private AttackPanel2 pnlAttack2;
    private ShiftPanel pnlShift;

    /* pnlPlayer reference to class that manages the panel of player and his activities during his turn
       player    reference to class that manages the player and his detail
       mapPanel  reference to class that manages the game world map table*/
    private PlayerPanel pnlPlayer;
    private Player player;
    private MapPanel mapPanel;

    /**
     * @param playerPanel  reference to class that manages the panel of player and his activities during his turn
     * @param playerCurr   reference to class that manages the player and his detail
     */
    public AttackManagerPanel(PlayerPanel playerPanel, Player playerCurr){

        this.pnlPlayer = playerPanel;
        this.player = playerCurr;
        this.player.setNArmiesForTurn();

        this.setLayout(new BorderLayout());

        pnlReinforce = new ReinforcePanel(this, player);
        pnlReinforce.setPanelComponent_ON();
        this.add(pnlReinforce, BorderLayout.NORTH);

        JPanel pnlStateAttack = new JPanel(new BorderLayout());
        pnlAttack1 = new AttackPanel1(this, player);
        pnlStateAttack.add(pnlAttack1, BorderLayout.NORTH);

        pnlAttack2 = new AttackPanel2(this, player);
        pnlStateAttack.add(pnlAttack2, BorderLayout.CENTER);
        this.add(pnlStateAttack, BorderLayout.CENTER);

        pnlShift = new ShiftPanel(this, player);
        this.add(pnlShift, BorderLayout.SOUTH);

        this.setVisible(false);
    }

    /**
     * Method to reset some component of the panel when the system set the player panel as turn after the first
     */
    public void setResetComponent(){
        this.pnlAttack2.resetComponent();
        this.pnlShift.resetComponent();
    }

    /**
     * Method to enable the first attack section after the reinforce turn
     */
    public void setPnlAttack1(){
        this.pnlReinforce.setPanelComponent_OFF();
        this.pnlAttack1.setPanelComponent_ON();
    }

    /**
     * Method to enable the second attack section after the player chose to attack
     */
    public void setPnlAttack2(){
        this.pnlAttack1.setPanelComponent_OFF();
        this.pnlAttack2.setPanelComponent_ON();
    }

    /**
     * Method to enable the shift section after the player ends the attack of he chose to not attack
     */
    public void setPnlShift(){
        this.pnlAttack2.setPanelComponent_OFF();
        this.pnlAttack1.setPanelComponent_OFF();
        this.pnlShift.setPanelComponent_ON();
    }

    /**
     * Method to disable all panel when the player ends his turn
     */
    public void setEndTurn(){
        this.pnlShift.setPanelComponent_OFF();
    }

    /**
     * Method to get a reference to the class that manage the general player panel
     *
     * @return  reference to playerPanel class
     */
    public PlayerPanel getPnlPlayer(){ return this.pnlPlayer;}

    /**
     * Method to get the reference to MapPanel class
     *
     * @return  reference to mapPanel class
     */
    public MapPanel getMapPanel() {
        return mapPanel;
    }

    /**
     * Method to set the current reference to MapPanel class
     *
     * @param mp    the reference to mapPanel class
     */
    public void setMapPanelReference(MapPanel mp){this.mapPanel = mp;}

    /**
     * Method to get a reference to the class that manage the component of Reinforce turn
     *
     * @return  reference to reinforce class
     */
    public ReinforcePanel getPnlReinforce(){return pnlReinforce;}

    /**
     * Method to get a reference to the class that manage the component of Attack turn
     *
     * @return   reference to attack class
     */
    public AttackPanel2 getPnlAttack2(){return pnlAttack2;}

    /**
     * Method to get a reference to the class that manage the component of Shift turn
     *
     * @return   reference to shift class
     */
    public ShiftPanel getPnlShift(){return pnlShift;}
}
