package risk.attack;

import risk.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Class for the manage the first match of attack, when the player choose if attack his opponent or end his turn
 * and shift some armies between his territories on the game table.
 *
 * @author Masi Elisabetta
 *
 */
public class AttackPanel1 extends JPanel implements StateGamePanel {

    /* pnlAttackManager reference to class that manages the panel of player and his activities during his turn
       player    reference to class that manages the player and his detail */
    private AttackManagerPanel pnlAttackManager;
    private Player player;

    // Components of panel to allow the activities of player
    private JPanel pnlDetail, pnlButton;
    private JButton btnAttack, btnEndTurn;

    /**
     * @param attackManagerPanel reference to panel of the current player
     * @param playerCurr  reference to player and his detail
     */
    public AttackPanel1(AttackManagerPanel attackManagerPanel, Player playerCurr){

        this.pnlAttackManager = attackManagerPanel;
        this.player = playerCurr;

        this.setLayout(new BorderLayout());
        this.setBackground(Color.LIGHT_GRAY);

        JLabel lblTitle = new JLabel("2 - ATTACCO");
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 14));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lblTitle, BorderLayout.NORTH);

        this.pnlDetail = new JPanel(new BorderLayout());
        JLabel lblDetail = new JLabel("Cosa preferisci fare?");
        lblDetail.setHorizontalAlignment(SwingConstants.CENTER);
        this.pnlDetail.add(lblDetail, BorderLayout.CENTER);

        this.pnlButton = new JPanel(new BorderLayout());
        this.btnAttack = new JButton("ATTACCO");
        btnAttack.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                pnlAttackManager.setPnlAttack2();
                btnAttack.setEnabled(false);
                btnEndTurn.setEnabled(false);
                pnlAttackManager.getMapPanel().enabledAllButtons(true);

                player.sendMessage("ATTACCO");
            }
        });
        btnAttack.setEnabled(false);
        this.pnlButton.add(btnAttack, BorderLayout.WEST);

        this.btnEndTurn = new JButton("FINE ATTACCO");
        btnEndTurn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlAttackManager.setPnlShift();
                btnAttack.setEnabled(false);
                btnEndTurn.setEnabled(false);
                player.sendMessage("FINE ATTACCO");
            }
        });
        btnEndTurn.setEnabled(false);
        this.pnlButton.add(btnEndTurn, BorderLayout.EAST);

        this.pnlDetail.add(pnlButton, BorderLayout.SOUTH);
        this.add(pnlDetail, BorderLayout.CENTER);
    }

    /**
     * Method to enable the panel when the player ends the renforce round
     */
    @Override
    public void setPanelComponent_ON(){
        this.setBackground(Color.RED);
        this.pnlDetail.setBackground(Color.RED);
        this.pnlButton.setBackground(Color.RED);
        this.btnAttack.setEnabled(true);
        this.btnEndTurn.setEnabled(true);
    }

    /**
     * Method to reset some component of the panel when the system set the player panel as turn after the first
     */
    @Override
    public void resetComponent() {

    }

    /**
     * Method to disable the panel when the player choose if attack or end his round
     */
    @Override
    public void setPanelComponent_OFF(){
        this.setBackground(Color.LIGHT_GRAY);
        this.pnlDetail.setBackground(Color.LIGHT_GRAY);
        this.pnlButton.setBackground(Color.LIGHT_GRAY);
    }
}