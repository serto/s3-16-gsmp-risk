package risk.attack;

import esScala.State;
import message.SendShiftArmies;
import risk.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Class to manage the Shift turn as final section of Attack round.
 * Whenever that the player ends the attack round, as final activity he can do a only shift from his territory
 * to his other territory. He can also choose the number of armies to shift, thanks to the relative buttons "plus" and "minus".
 *
 * After the shift the player ends the attack turn and selecting the button of "end turn" the client sends a new message to the server
 * which updates the game state of two players.
 *
 * Every time the player selects one of the buttons in the panel, the client sends a new message of server with some details of shift operation.
 * The server sends to defencing player the detail and updates his panels.
 *
 * At the end of Attack round the two players reverse their game states of "Attack" and "Defence", so update the relative panels and maps.
 *
 * @author Masi Elisabetta
 */
public class ShiftPanel extends JPanel implements StateGamePanel {

    /* pnlAttackManager reference to class that manages the panel of player and his activities during his turn
       player    reference to class that manages the player and his detail */
    private AttackManagerPanel pnlAttackManager;
    private Player player;

    // Components of panel to allow the activities of player
    private JPanel pnlDetail, pnlDetailFrom, pnlDetailTo, pnlNArmies;
    private JLabel lblFrom, lblTo, lblNArmies;
    private JButton btnShift, btnEndRound, btnFrom, btnTo, btnMinus, btnPlus;

    /* nArmiesOfTerritory   number armies of territory selected as country from which do the shift
       nArmiesToShift       number armies that player chooses to shift between the territories  */
    private int nArmiesOfTerritory, nArmiesToShift;


    /**
     * @param attackManagerPanel   reference to panel of the current player
     * @param playerCurr  reference to player and his detail
     */
    public ShiftPanel(AttackManagerPanel attackManagerPanel, Player playerCurr){

        this.pnlAttackManager = attackManagerPanel;
        this.player = playerCurr;

        this.setLayout(new BorderLayout());
        this.setBackground(Color.LIGHT_GRAY);

        JLabel lblTitle = new JLabel("4 - SPOSTAMENTO");
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 14));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lblTitle, BorderLayout.NORTH);

        this.pnlDetail = new JPanel(new BorderLayout());
        this.pnlDetailFrom = new JPanel(new BorderLayout());
        this.btnFrom = new JButton("DA");
        this.btnFrom.setHorizontalAlignment(SwingConstants.CENTER);
        this.btnFrom.setEnabled(false);
        this.pnlDetailFrom.add(btnFrom, BorderLayout.WEST);
        this.btnFrom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlAttackManager.getMapPanel().getMapManager().state().setState_(State.SHIFT_FROM());
                btnTo.setEnabled(true);
            }
        });
        this.lblFrom = new JLabel(" ");
        this.pnlDetailFrom.add(lblFrom, BorderLayout.CENTER);
        this.pnlDetail.add(pnlDetailFrom, BorderLayout.NORTH);

        this.pnlDetailTo = new JPanel(new BorderLayout());
        this.btnTo = new JButton("A");
        this.btnTo.setHorizontalAlignment(SwingConstants.CENTER);
        this.btnTo.setEnabled(false);
        this.pnlDetailTo.add(btnTo, BorderLayout.WEST);

        this.btnTo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlAttackManager.getMapPanel().getMapManager().state().setState_(State.SHIFT_TO());
            }
        });
        this.lblTo = new JLabel(" ");
        this.pnlDetailTo.add(lblTo, BorderLayout.CENTER);
        this.pnlDetail.add(pnlDetailTo, BorderLayout.CENTER);

        this.pnlNArmies = new JPanel(new BorderLayout());
        JLabel lblDetailArmies = new JLabel("con quante armate?");
        lblDetailArmies.setHorizontalAlignment(SwingConstants.CENTER);
        pnlNArmies.add(lblDetailArmies, BorderLayout.NORTH);

        this.btnMinus = new JButton("-");
        this.btnMinus.setEnabled(false);
        this.btnMinus.setPreferredSize(new Dimension(50,50));
        this.btnMinus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!btnPlus.isEnabled()){
                    btnPlus.setEnabled(true);}
                if(nArmiesToShift > 1){
                    nArmiesToShift--;
                    lblNArmies.setText(String.valueOf(nArmiesToShift--));
                }else{
                    btnMinus.setEnabled(false);}
            }
        });
        pnlNArmies.add(this.btnMinus, BorderLayout.WEST);

        this.lblNArmies = new JLabel("");
        this.lblNArmies.setHorizontalAlignment(SwingConstants.CENTER);
        pnlNArmies.add(lblNArmies);

        this.btnPlus = new JButton("+");
        this.btnPlus.setEnabled(false);
        this.btnPlus.setPreferredSize(new Dimension(50,50));
        this.btnPlus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!btnMinus.isEnabled()){
                    btnMinus.setEnabled(true);}
                if(nArmiesOfTerritory > nArmiesToShift){
                    nArmiesToShift++;
                    lblNArmies.setText("" + nArmiesToShift);
                } else{
                    btnPlus.setEnabled(false);}
            }
        });
        pnlNArmies.add(this.btnPlus, BorderLayout.EAST);
        this.pnlDetail.add(pnlNArmies, BorderLayout.SOUTH);
        this.add(pnlDetail, BorderLayout.CENTER);

        JPanel pnlButton = new JPanel(new BorderLayout());
        this.btnShift = new JButton("SPOSTA");
        btnShift.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlAttackManager.getMapPanel().getMapManager().updateArmiesTerritory(nArmiesToShift);
                btnShift.setEnabled(false);
                btnFrom.setEnabled(false);
                btnTo.setEnabled(false);
                btnPlus.setEnabled(false);
                btnMinus.setEnabled(true);
                enabledBtnShiftPnl(false);

                player.sendMessage(new SendShiftArmies(pnlAttackManager.getMapPanel().getMapManager().territoryShiftFrom().idTerritory(), pnlAttackManager.getMapPanel().getMapManager().territoryShiftTo().idTerritory(), pnlAttackManager.getMapPanel().getTerritories()));
            }
        });
        this.btnShift.setEnabled(false);
        pnlButton.add(btnShift, BorderLayout.WEST);

        this.btnEndRound = new JButton("FINE TURNO");
        this.btnEndRound.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlAttackManager.setEndTurn();
                pnlAttackManager.getMapPanel().getMapManager().state().setState_(State.END_TURN());
                pnlAttackManager.getMapPanel().enabledAllButtons(false);
                btnShift.setEnabled(false);
                btnFrom.setEnabled(false);
                btnTo.setEnabled(false);
                enabledBtnShiftPnl(false);

                player.sendMessage("FINE_TURNO");
                pnlAttackManager.getPnlPlayer().setStateGame(PlayerPanel.DEFENCE);
            }
        });
        this.btnEndRound.setEnabled(false);
        pnlButton.add(btnEndRound, BorderLayout.EAST);
        this.add(pnlButton, BorderLayout.SOUTH);

    }

    /**
     * Method to set the label with the name of territory that player selects on the map as from shift territory
     *
     * @param nameCountryFrom the name of Country
     */
    public void setLblFrom(String nameCountryFrom){
        this.lblFrom.setText(nameCountryFrom);
    }

    /**
     * Method to set the label with the name of territory that player selects on the map as to shift territory
     *
     * @param nameCountryTo the name of Country
     */
    public void setLblTo(String nameCountryTo){
        this.lblTo.setText(nameCountryTo);
    }

    /**
     * Method to set the label that show the number of armies of first territory.
     * This allow player to choose with how many armies shifting the other territory.
     * Setting the variables the class manages the add or less of number armies allowing player a better chose.
     *
     * @param nArmies  number of armies to first territory
     */
    public void setLblNArmies (int nArmies){
        this.nArmiesToShift = nArmies - 1;
        this.nArmiesOfTerritory = nArmies - 1;

        this.lblNArmies.setText("" + this.nArmiesToShift);
    }

    /**
     * Method to enable and disable the minus and plus buttons according to the state of game.
     * When the player choose the territories for the shift the buttons are enabled, otherwise they are disable.
     *
     * @param newStateButton   new state for the button of panel
     */
    public void enabledBtnShiftPnl(boolean newStateButton){
        this.btnMinus.setEnabled(newStateButton);
        this.btnPlus.setEnabled(newStateButton);
    }

    /**
     * Method to enable the panel when the player is in shift turn
     */
    @Override
    public void setPanelComponent_ON() {
        this.setBackground(Color.RED);
        this.pnlDetail.setBackground(Color.RED);
        this.pnlDetailFrom.setBackground(Color.RED);
        this.pnlDetailTo.setBackground(Color.RED);
        this.pnlNArmies.setBackground(Color.RED);
        this.btnFrom.setEnabled(true);
        this.btnShift.setEnabled(true);
        this.btnEndRound.setEnabled(true);
    }

    /**
     * Method to reset some component of the panel when the system set the player panel as turn after the first
     */
    @Override
    public void resetComponent() {
        this.lblTo.setText(" ");
        this.lblFrom.setText(" ");
        this.lblNArmies.setText(" ");
    }

    /**
     * Method to disable the panel when the player choose not shift so end his round, or when he ends his shift and ends his turn
     */
    @Override
    public void setPanelComponent_OFF() {
        this.setBackground(Color.LIGHT_GRAY);
        this.pnlDetail.setBackground(Color.LIGHT_GRAY);
        this.pnlDetailFrom.setBackground(Color.LIGHT_GRAY);
        this.pnlDetailTo.setBackground(Color.LIGHT_GRAY);
        this.pnlNArmies.setBackground(Color.LIGHT_GRAY);
    }
}