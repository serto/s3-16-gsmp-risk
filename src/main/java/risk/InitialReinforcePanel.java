package risk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Class to manage the initial phase of the game.
 * Every player must distribute a static number of armies between his territories in the map.
 * To simplify the game, the sistem add to every terrritory one arm so to limited the errories by player who
 * could forget
 *
 * @author Masi Elisabetta
 */
public class InitialReinforcePanel extends JPanel{

    /*pnlPlayer reference to class that manages the panel of player and his activities during his turn
      player    reference to class that manages the player and his detail */
    private PlayerPanel pnlPlayer;
    private Player player;

    //components of panel to show the activities of player
    private JLabel lblNumCurrArmies;
    private JButton btnNext;

    /**
     * @param playerPanel   reference to class that manages the panel of player and his activities during his turn
     * @param playerCurr    reference to class that manages the player and his detail
     */
    public InitialReinforcePanel(PlayerPanel playerPanel, Player playerCurr){

        this.pnlPlayer = playerPanel;
        this.player = playerCurr;

        this.setLayout(new BorderLayout());

        JLabel lblTitle = new JLabel("DISTRIBUISCI LE ARMATE");
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 14));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lblTitle, BorderLayout.NORTH);

        JPanel pnlDetail = new JPanel(new BorderLayout());
        JLabel lblNArmiesDistr = new JLabel("Numero armate da distribuire");
        lblNArmiesDistr.setHorizontalAlignment(SwingConstants.CENTER);
        lblNArmiesDistr.setFont(new Font("Arial",  Font.PLAIN, 13));
        pnlDetail.add(lblNArmiesDistr, BorderLayout.NORTH);

        this.lblNumCurrArmies = new JLabel(" " + player.getnArmiesInit());
        this.lblNumCurrArmies.setHorizontalAlignment(SwingConstants.CENTER);
        this.lblNumCurrArmies.setFont(new Font("Arial",  Font.BOLD, 14));
        pnlDetail.add(lblNumCurrArmies, BorderLayout.CENTER);

        JPanel pnlDetailChoose = new JPanel(new BorderLayout());
        JLabel lblChoose1 = new JLabel("clicca sul territorio dove ");
        lblChoose1.setHorizontalAlignment(SwingConstants.CENTER);
        lblChoose1.setFont(new Font("Arial",  Font.PLAIN, 13));
        JLabel lblChoose2 = new JLabel("vuoi posizionare le tue armate");
        lblChoose2.setHorizontalAlignment(SwingConstants.CENTER);

        lblChoose2.setFont(new Font("Arial",  Font.PLAIN, 13));
        pnlDetailChoose.add(lblChoose1, BorderLayout.CENTER);
        pnlDetailChoose.add(lblChoose2, BorderLayout.SOUTH);
        pnlDetail.add(pnlDetailChoose, BorderLayout.SOUTH);
        this.add(pnlDetail, BorderLayout.CENTER);

        this.btnNext = new JButton("Next");
        btnNext.setEnabled(false);
        btnNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pnlPlayer.setStateGame(PlayerPanel.ATTACK);
                pnlPlayer.getMapPanel().enabledButtons(true);
                player.sendMessage("DEFENCE");
            }
        });
        this.add(btnNext, BorderLayout.SOUTH);
    }

    /**
     * Method to set the label with number of armies that the player must distribute between his territories
     * during the init turn of the game
     *
     * @param nArmiesCurr   number armies to distribute
     */
    public void setLblNArmiesTurn(int nArmiesCurr){ this.lblNumCurrArmies.setText(""+nArmiesCurr);}


    /**
     * Method to enable the button "next" when the player ends to distribute the armies
     */
    public void setBtnNext(){this.btnNext.setEnabled(true);}
}
