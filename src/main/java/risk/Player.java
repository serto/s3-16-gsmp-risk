package risk;

import akka.actor.ActorRef;
import risk.target.Target;
import scala.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for the management of the player and all his aspects during the all game.
 * This class implements Serializable because it will be sent into a message by the system.
 *
 * @author Genghini Luca
 */

public class Player implements Serializable {

    private int playerID, nArmiesInit = 20, nArmiesForTurn;
    private List<Integer> territoryList = new ArrayList<>();
    private List<Integer> bonusList = new ArrayList<>();
    private List<Integer> cannons = new ArrayList<>(), infantrymen = new ArrayList<>(), horses = new ArrayList<>(), oneForType = new ArrayList<>();
    private boolean jollyIsPresent = false;

    private Target target;

    private ActorRef actor;

    private RiskGUI game;

    /**
     * method to set the initial options of the player
     *
     * @param actor the reference of the actor to send the message
     * @param playerID the id of the player
     */
    public void setOption(ActorRef actor, int playerID){
        this.playerID = playerID;
        this.actor = actor;
    }

    /**
     * method to get player id
     *
     * @return the player id
     */
    public int getPlayerID(){return this.playerID;}

    /**
     * method for adding a territory to the list of the territories owned by the player
     *
     * @param territory the territory to be added at the territory list of the player
     */
    public void addTerritory(int territory){
        this.territoryList.add(new Integer(territory));
    }

    /**
     * method for subtracting a territory from the list of territories owned by the player
     *
     * @param territory the territory to be subtract from the territory list of the player
     */
    public void subtractTerritory(int territory){
        this.territoryList.remove(new Integer(territory));
    }

    /**
     * method to get the number of the territories owned by the player
     *
     * @return the number of the territories owned by the player
     */
    public int getnTerritories(){return this.territoryList.size(); }

    /**
     * method to get the territory list of the player
     *
     * @return the territory list of the player
     */
    public List<Integer> getTerritoryList(){return this.territoryList;}

    /**
     * method for setting the initial number of the armies keeping in mind of the all possible bonuses obtainable from the player
     */
    public void setNArmiesForTurn(){
        System.out.println(this.territoryList.toString() + " Listone pazzo");
        this.nArmiesForTurn = this.getnTerritories()/3;
        if(!this.bonusList.isEmpty()){
            int bonus = this.bonusList.get(this.bonusList.size()-1);
            switch (bonus){
                case Bonus.CANNON:
                    this.cannons.add(bonus);
                    if(!this.oneForType.contains(bonus)){
                        this.oneForType.add(bonus);
                    }
                    break;
                case Bonus.INFANTRYMAN:
                    this.infantrymen.add(bonus);
                    if(!this.oneForType.contains(bonus)){
                        this.oneForType.add(bonus);
                    }
                    break;
                case Bonus.HORSE:
                    this.horses.add(bonus);
                    if(!this.oneForType.contains(bonus)){
                        this.oneForType.add(bonus);
                    }
                    break;
                case Bonus.JOLLY:
                    if(!this.jollyIsPresent){
                        this.cannons.add(bonus);
                        this.infantrymen.add(bonus);
                        this.horses.add(bonus);
                        this.jollyIsPresent = true;
                    }
                    break;
            }
            this.nArmiesForTurn += this.checkBonus();
        }

        this.checkContinentsBonus();
    }

    /**
     * method to count the number of bonus armies
     *
     * @return the number of bonus armies
     */
    private int checkBonus() {
        if(this.cannons.size() ==3){
            if(this.cannons.contains(Bonus.JOLLY)){
                this.removeJolly();
                this.bonusAchieved(this.cannons);
                this.cannons.clear();
                this.jollyIsPresent = false;
                return Bonus.ARMIES_BONUS_JOLLY;
            }
            else{
                this.bonusAchieved(this.cannons);
                this.cannons.clear();
                return Bonus.ARMIES_BONUS_CANNON;
            }
        }
        if(this.infantrymen.size() == 3){
            if(this.infantrymen.contains(Bonus.JOLLY)){
                this.removeJolly();
                this.bonusAchieved(this.infantrymen);
                this.infantrymen.clear();
                this.jollyIsPresent = false;
                return Bonus.ARMIES_BONUS_JOLLY;
            }
            else{
                this.bonusAchieved(this.infantrymen);
                this.infantrymen.clear();
                return Bonus.ARMIES_BONUS_INFANTRYMAN;
            }
        }
        if(this.horses.size() == 3){
            if(this.horses.contains(Bonus.JOLLY)){
                this.removeJolly();
                this.bonusAchieved(this.horses);
                this.horses.clear();
                this.jollyIsPresent = false;
                return Bonus.ARMIES_BONUS_JOLLY;
            }
            else{
                this.bonusAchieved(this.horses);
                this.horses.clear();
                return Bonus.ARMIES_BONUS_HORSE;
            }
        }
        if(this.oneForType.size() == 3){
            this.bonusAchieved(this.oneForType);
            this.oneForType.clear();
            return Bonus.ARMIES_BONUS_ONEFORTYPE;
        }
        return 0;
    }

    /**
     * method for the management of the reached bonus
     *
     * @param tmpList the list of the reached bonus
     */
    private void bonusAchieved(List<Integer> tmpList) {
        for(int bonus : tmpList){
            this.bonusList.remove(new Integer(bonus));
        }
    }

    /**
     * method for removing jolly from all the list of bonus
     */
    private void removeJolly() {
        this.cannons.remove(new Integer(Bonus.JOLLY));
        this.infantrymen.remove(new Integer(Bonus.JOLLY));
        this.horses.remove(new Integer(Bonus.JOLLY));
    }

    /**
     * method to check if the player owns an entire continent;
     * if so add the corresponding number of armies to the number of initial armies for the turn
     */
    private void checkContinentsBonus(){
        if(this.territoryList.containsAll(Continent.ASIAterritoryList)){
            this.nArmiesForTurn += 7;
        }

        if(this.territoryList.containsAll(Continent.NORTH_AterritoryList) ||
                this.territoryList.containsAll(Continent.EUROPEterritoryList)){
            this.nArmiesForTurn += 5;
        }

        if(this.territoryList.containsAll(Continent.AFRICAterritoryList)){
            this.nArmiesForTurn += 3;
        }

        if(this.territoryList.containsAll(Continent.AUSTRALIAterritoryList) ||
                this.territoryList.containsAll(Continent.SOUTH_AterritoryList)){
            this.nArmiesForTurn += 2;
        }
    }

    /**
     * method to get the number of the initial armies for the turn
     *
     * @return the number of the initial armies for the turn
     */
    public int getNArmies(){return this.nArmiesForTurn;}

    /**
     * method for subtracting 1 army from the initial armies for the turn
     */
    public void setNArmies(){this.nArmiesForTurn--;}

    /**
     * method to get the number of armies to distribute at start of the game
     *
     * @return the number of armies to distribute at start of the game
     */
    public int getnArmiesInit(){return this.nArmiesInit;}

    /**
     * method for subtracting 1 army from the armies to distribute at the start of the game
     */
    public void setNArmiesInit(){this.nArmiesInit--;}

    /**
     * method for populating the list of the initial territories of the player
     *
     * @param list the list of the initial territories of the player
     */
    public void populateTerritoriesList(List<?> list){
        list.forEach(e -> territoryList.add((Integer) e));
        System.out.println(territoryList.toString() + " lista all'inizio");
    }

    /**
     * method to get the target of the player
     *
     * @return the target of the player
     */
    public Target getTarget(){
        return this.target;
    }

    /**
     * method for setting the target that the player has to reach
     *
     * @param target the target that the player has to reach
     */
    public void setTarget(Target target){
        this.target = target;
    }

    /**
     * method for adding a bonus to the player bonus list
     *
     * @param bonus the bonus to add at the player bonus list
     */
    public void addBonus(int bonus){
        this.bonusList.add(bonus);
    }

    /**
     * method to get the player bonus list
     *
     * @return the player bonus list
     */
    public List<Integer> getBonusList(){
        return this.bonusList;
    }

    /**
     * method for setting the frame of the game
     *
     * @param game the frame of the game
     */
    public void setGame(RiskGUI game){
        this.game = game;
    }

    /**
     * method for sending messages to the actor reference of the player
     *
     * @param msg the message to send at the actor reference of the player
     */
    public void sendMessage(Object msg){
        actor.tell(msg, null);
    }

}