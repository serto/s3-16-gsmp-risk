package risk.target;

import risk.Continent;
import risk.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to represent the target of conquer 3 different continents.
 *
 * @author Genghini Luca
 */

public class TargetThreeContinents implements Target {

    // description of the target
    private static final String descr = "Conquista 3 continenti";

    private List<Integer> terList = new ArrayList<>();
    private int nContinent = 0;

    /**
     * method to verify if the target is reached
     *
     * @param player reference to player and his details
     * @return       true if the player reaches is target
     */
    @Override
    public boolean checkTarget(Player player) {
        this.terList = player.getTerritoryList();
        if(this.goalAchieved()){
            return true;
        }
        return false;
    }

    /**
     * ricorsive method to check if the player reaches the target
     *
     * @return true if the player reaches the goal
     */
    private boolean goalAchieved(){
        if(terList.containsAll(Continent.NORTH_AterritoryList)){
            if(this.countContinent(Continent.NORTH_AterritoryList)){
                return true;
            }else{
                this.goalAchieved();
            }
        }else if( terList.containsAll(Continent.SOUTH_AterritoryList)){
            if(this.countContinent(Continent.SOUTH_AterritoryList)){
                return true;
            }else{
                this.goalAchieved();
            }
        }else if(terList.containsAll(Continent.AFRICAterritoryList)){
            if(this.countContinent(Continent.AFRICAterritoryList)){
                return true;
            }else{
                this.goalAchieved();
            }
        }else if(terList.containsAll(Continent.EUROPEterritoryList)){
            if(this.countContinent(Continent.EUROPEterritoryList)){
                return true;
            }else{
                this.goalAchieved();
            }
        }else if(terList.containsAll(Continent.ASIAterritoryList)){
            if(this.countContinent(Continent.ASIAterritoryList)){
                return true;
            }else{
                this.goalAchieved();
            }
        }else if(terList.containsAll(Continent.AUSTRALIAterritoryList)){
            if(this.countContinent(Continent.AUSTRALIAterritoryList)){
                return true;
            }else{
                this.goalAchieved();
            }
        }
        return false;
    }

    /**
     * method to verify if the player wins 3 continents. This method is called everytime all the territories of a continent are owned
     * by the player.
     *
     * @param continent the list with all the territory ids of a continent
     * @return          true if the number of continents owned by the player is 3, false in the other cases
     */
    private boolean countContinent(List<Integer> continent){
        this.nContinent++;
        this.terList.removeAll(continent);
        if(this.nContinent == 3){
            return true;
        }
        return false;
    }

    /**
     * method for getting the description of the target
     *
     * @return the description of the target
     */
    public String getDescr(){
        return this.descr;
    }
}
