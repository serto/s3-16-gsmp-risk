package risk.target;

import risk.Continent;
import risk.Player;

/**
 * Class to represent the target of conquer South America and Europe.
 *
 * @author Genghini Luca
 */

public class TargetSA_E implements Target {

    // description of the target
    private static final String descr = "Conquista il SUD AMERICA e l'EUROPA";

    /**
     * method to verify if the target is reached
     *
     * @param player reference to player and his details
     * @return       true if the player reaches is target
     */
    @Override
    public boolean checkTarget(Player player) {
        if(player.getTerritoryList().containsAll(Continent.SOUTH_AterritoryList) &&
                player.getTerritoryList().containsAll(Continent.EUROPEterritoryList)){
            return true;
        }

        return false;
    }

    /**
     * method for getting the description of the target
     *
     * @return the description of the target
     */
    public String getDescr(){
        return this.descr;
    }
}
