package risk.target;

import risk.Player;

/**
 * Class to represent the target of conquer 30 territories. When the player wins the 30th territory, wins the game.
 *
 * @author Genghini Luca
 */

public class Target30Territories implements Target {

    // description of the target
    private static final String descr = "Conquista 30 Territori";

    /**
     * method to verify if the target is reached
     *
     * @param player reference to player and his details
     * @return       true if the player reaches is target
     */
    @Override
    public boolean checkTarget(Player player) {
        if(player.getnTerritories() == 30){
            return true;
        }
        return false;
    }

    /**
     * method for getting the description of the target
     *
     * @return the description of the target
     */
    public String getDescr(){
        return this.descr;
    }
}
