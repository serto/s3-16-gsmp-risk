package risk.target;

import risk.Player;
import scala.Serializable;

/**
 * Interface to manage the different bonuses which player can get.
 *
 * @author Genghini Luca
 */
public interface Target extends Serializable {

    /**
     * method to verify if the target is reached
     *
     * @param player reference to player and his details
     * @return       true if the player reaches is target
     */
    boolean checkTarget(Player player);

    /**
     * method for getting the description of the target
     *
     * @return the description of the target
     */
    String getDescr();
}
