package risk;

import message.DeleteTerritoryInList;

import java.util.List;

/**
 * Class to manager the structure and the functions of map panel for the table game. It connect the map panel with the
 * player panel, to allow a better management of system.
 * The management of map panel is composed by some state: one for each state of game.
 * Whenever that a player selects a button in the map according to the state of game, the system executes
 * a method to set the different components in the panels.
 *
 * @author Masi Elisabetta
 */

public class MapManagerOld {

    //the game is in the initial reinforce
    public static final int INIT_REINFORCE = 0;
    //the game is in the attack turn
    public static final int REINFORCE = 1;
    public static final int ATTACKING = 2;
    public static final int TERRITORY_DEFENCE = 3;
    public static final int TERRITORY_ATTACKING = 4;
    public static final int DICES = 5;
    //the game is in the shift turn
    public static final int SHIFT_FROM = 6;
    public static final int SHIFT_TO = 7;
    //the game is in the exchange turn between the player
    public static final int END_TURN = 8;

    private int state = INIT_REINFORCE;

    /*  mapManager reference to class to manage the functions on the world map
      * pnlPlayer  reference to class that manages the panel of player and his activities during his turn
      * player     reference to class that manages the player and his detail */
    private MapPanel mapPanel;
    private Player player;
    private PlayerPanel playerPanel;

    //id territory that defence, attack, shift and a count to save the number of jolly
    private int terDefence, terAttack, terShiftFrom, terShiftTo, countJolly=0;
    private boolean firstConquest, hasConquered;

    /**
     * @param mapPanel      reference to class to manage the functions on the world map
     * @param player        reference to class that manages the player and his detail
     * @param playerPanel   reference to class that manages the panel of player and his activities during his turn
     */
    public MapManagerOld(MapPanel mapPanel, Player player, PlayerPanel playerPanel){
        this.mapPanel = mapPanel;
        this.player = player;
        this.playerPanel = playerPanel;
    }

    /**
     * Method to get the current state of map game
     *
     * @return  the current state of map game
     */
    public int getState(){return this.state;}

    /**
     * Method to set the new state of map game
     *
     * @param newState  new state of map game
     */
    public void setState(int newState){this.state = newState;}

    /**
     * Method to menage the position of territory.
     * According to the image world map, the system has combined with each territory an X and Y coordination to
     * draw its relative button in the correct position in the map.
     * The method return an array of int with the coordination of territory.
     *
     * @param idTer     id territory to draw in the map
     * @return          an array for the coordination of territory in the map
     */
    public int[] getPositionTerritory(int idTer){
        int[] cord = new int[2];

        cord[0] = mapPanel.getTerritories().get(idTer).getX();
        cord[1] = mapPanel.getTerritories().get(idTer).getY();

        return cord;
    }

    /* -----------------------------------------------------------------------------------------------------------

                                        methods for INIT distribution

    ----------------------------------------------------------------------------------------------------------- */
    /**
     * Method to check the distribute of armies in the territory during the initial reinforce turn
     * If the number of armies to distribute by player is not zero and the territory selected is own of player
     * the method add a new arm of territory and sub an arm to number of armies to already distribute by player.
     * If the player distributed all armies, the client sends to server a new message and disable all button, and
     * he wait that the other player finishes the distribution.
     *
     * @param idTer     id territory selected
     * @return          new number of armies of the territory
     */
    public int distributionArmies(int idTer){
        if(player.getnArmiesInit() != 0) {
            if (mapPanel.getTerritories().get(idTer).getPlayerOwner().getPlayerID() == player.getPlayerID()) {
                mapPanel.addArmiesToTerritory(idTer, 1);
                player.setNArmiesInit();
            } else {
               mapPanel.viewMessage("Attenzione non è un tuo territorio");
            }
        }
        if(player.getnArmiesInit() == 0) {
            player.sendMessage("Press_next");
            playerPanel.getMapPanel().enabledAllButtons(false);
            state = REINFORCE;
        }
        return mapPanel.getTerritories().get(idTer).getArmies();
    }

    /**
     * Method to set the label in the player panel in the InitReinforce section with new player number of armies
     */
    public void setNArmiesToINIT(){playerPanel.getPnlInitReinforce().setLblNArmiesTurn(player.getnArmiesInit());}

      /* -----------------------------------------------------------------------------------------------------------

                                         methods for REINFORCE round

    ----------------------------------------------------------------------------------------------------------- */
    /**
     * Method to check the distribute of armies in the territory during the reinforce turn in the attack match
     * If the number of player armies to distribute is not zero and the territory selected is own of player
     * the method add a new arm of territory and sub an arm to number of armies to already distribute by player.
     * If the player distributed all the armies the system enables the button next in the reinforce panel to allow player
     * to choose if attack.
     *
     * @param idTer     id territory selected
     * @return          new number of armies of the territory
     */
    public int setNArmiesToReinforce(int idTer){
        if(!checkNArmiesPlayerToDistribute()) {
            if (mapPanel.getTerritories().get(idTer).getPlayerOwner().getPlayerID() == player.getPlayerID()) {
                mapPanel.addArmiesToTerritory(idTer, 1);
                player.setNArmies();
            } else {
                mapPanel.viewMessage("Attenzione non è un tuo territorio");
            }
        }
        if(checkNArmiesPlayerToDistribute()) {
            playerPanel.getPnlAttackManager().getPnlReinforce().setBtnNext();
            state = ATTACKING;
        }
        return mapPanel.getTerritories().get(idTer).getArmies();
    }

    /**
     * Method to check if player distributed all his armies
     *
     * @return  true/false as result of check
     */
    private boolean checkNArmiesPlayerToDistribute(){ return player.getNArmies() == 0; }

    /**
     * Method to set the label in the player panel in the Reinforce section with new player number of armies
     */
    public void setNArmies_ReinforceTurn(){ playerPanel.getPnlAttackManager().getPnlReinforce().setLblNArmiesInThisTurn(player.getNArmies()); }

      /* -----------------------------------------------------------------------------------------------------------

                                            methods for ATTACKING round

         ----------------------------------------------------------------------------------------------------------- */
    /**
     * Method to set the label of player panel in the attack section with the name of territory selected in the map
     * as defencing component.
     * If player selects an correct territory the system enables the attacking section to allow player to select the
     * territory with which attack.
     *
     * @param idTerDefence      id territory selected as defence
     */
    public void setTerritoryDefencing(int idTerDefence){
        if(!this.checkTerritoryOwner(idTerDefence, player)){
            playerPanel.getPnlAttackManager().getPnlAttack2().setLblTo(mapPanel.getTerritories().get(idTerDefence).getName());
            this.terDefence = idTerDefence;
            playerPanel.getPnlAttackManager().getPnlAttack2().enableAttackSection();
        }else{
            mapPanel.viewMessage("attenzione devi selezionare un territorio dell'avversario!");
        }
    }

    /**
     * Method to get the number of armies with the player use to defence him
     * It is used in attackPanel1 to set the number of defence armies
     *
     * @return  number of armies of defencing territory
     */
    public int getArmiesTerritoryDefence(){return mapPanel.getTerritories().get(this.terDefence).getArmies();}

    /**
     * Method to set the label of player panel in the attack section with the name of territory selected in the map
     * as attacking component.
     * The method checks the owner of territory, if the territory selected is adjacent of defencing territory and if
     * the territory has a number of armies major of 1, to allow a correct execute of game.
     * Then it set the label of player panel in the attack section and it enables the button to allow player the dice roll.
     *
     * @param idTerAttack   id territory select as attack
     */
    public void setTerritoryAttacking(int idTerAttack){
        if(this.checkTerritoryOwner(idTerAttack, player)){
            if(checkTerritoryAdjacent(this.terDefence + 1, idTerAttack)){
                if(mapPanel.getTerritories().get(idTerAttack).getArmies() > 1) {
                    playerPanel.getPnlAttackManager().getPnlAttack2().setLblFrom(mapPanel.getTerritories().get(idTerAttack).getName());
                    playerPanel.getPnlAttackManager().getPnlAttack2().setLblNArmies(mapPanel.getTerritories().get(idTerAttack).getArmies());
                    playerPanel.getPnlAttackManager().getPnlAttack2().enabledBtnAttackPnl(true);
                    this.terAttack = idTerAttack;
                }else{
                    mapPanel.viewMessage("Attenzione devi selezionare un territorio con almeno 2 armate!");
                }
            }else {
                mapPanel.viewMessage("attenzione devi selezionare un territorio adiacente a quello attaccato!");
            }
        }else{
            mapPanel.viewMessage("attenzione devi selezionare un tuo territorio da cui attaccare!");
        }
    }

    /**
     * Method to check if the owner of territory selected is the current player
     *
     * @param idTer         id territory to check owner player
     * @param idPlayer      id current player
     * @return              result of check
     */
    private boolean checkTerritoryOwner(int idTer, Player idPlayer){
        return mapPanel.getTerritories().get(idTer).getPlayerOwner().getPlayerID() == idPlayer.getPlayerID();
    }

    /**
     * Method to check if attacking
     *
     * @param terTo     id attacking territory
     * @param terFrom   id defencing territory
     * @return          result of check
     */
    private boolean checkTerritoryAdjacent(int terTo, int terFrom){ return this.mapPanel.getTerritories().get(terFrom).getAdjacent().contains(terTo); }

    /**
     * Method to get the id attacking territory
     *
     * @return  id attacking territory
     */
    public int getTerAttack(){ return this.terAttack; }

    /**
     * Method to get the id defencing territory
     *
     * @return  id attacking territory
     */
    public int getTerDefence(){ return this.terDefence;}


    /**
     * Method to set the state of attack and defence territories removing the number of armies lost after dice roll.
     * If the attacking player won the defencing territory, the method:
     *    set this territory with:  a new player owner name and sub the territory by the relative list of defence player,
     *                            set a new number of its armies according to number of armies with which the player chose to attack and sub the equal number by attacking territory.
     *    check if the player current with the conquest of this territory.
     *  If is true the client sends a new message to server to show the winner frame and to inform the other player with the result of game
     *    add a new bonus for the player and manage the number of these
     *
     * In the end, the method recalls the method in mapPanel to update the number of armies in the territory and set the button in the map
     * with the new data.
     * Than the client sends a new message to server with the data of attack, to allow defencing player to know the result in this panel.
     *
     * @param nArmiesForAttack      number of armies with which the player chose to attack
     * @param nArmiesAttackLost     number of armies lost by attacking player
     * @param nArmiesDefenceLost    number of armies lost by defencing player
     */
    public void updateNArmiesAfterDices(int nArmiesForAttack, int nArmiesAttackLost, int nArmiesDefenceLost){

        //remove the armies lost in the territories
        mapPanel.removeArmiesToTerritory(terAttack, nArmiesAttackLost);
        mapPanel.removeArmiesToTerritory(terDefence, nArmiesDefenceLost);

        //the attack player won the defencing territory and set its armies and owner
        if(mapPanel.getTerritories().get(terDefence).getArmies() <= 0){
            player.sendMessage(new DeleteTerritoryInList(terDefence));
            mapPanel.setPlayerOwnerTerritory(terDefence, mapPanel.getTerritories().get(terAttack).getPlayerOwner());

            if (nArmiesForAttack > 3) {
                mapPanel.setArmiesToTerritory(terDefence, 3);
                mapPanel.removeArmiesToTerritory(terAttack, 3);
            } else {
                if(nArmiesForAttack == 1){
                    mapPanel.setArmiesToTerritory(terDefence, nArmiesForAttack);
                    mapPanel.removeArmiesToTerritory(terAttack, nArmiesForAttack);
                }else{
                    mapPanel.setArmiesToTerritory(terDefence, nArmiesForAttack - 1);
                    mapPanel.removeArmiesToTerritory(terAttack, nArmiesForAttack - 1);
                }
            }

            //check if the player is the winner of the game
            if(this.player.getTarget().checkTarget(mapPanel.getTerritories().get(terAttack).getPlayerOwner())){
                this.player.sendMessage("WINNER");
            }

            //manage the distribution of bonus
            this.hasConquered = true;

            if(this.firstConquest){
                if(this.player.getBonusList().size() < 7){
                    Bonus bonus = new Bonus();
                    if(bonus.getBonus() == Bonus.JOLLY){
                        this.countJolly++;
                        if(countJolly <= 2){
                            this.player.addBonus(bonus.getBonus());
                            this.firstConquest = false;
                        } else{
                            bonus.recalculateBonus();
                            this.player.addBonus(bonus.getBonus());
                            this.firstConquest = false;
                        }
                    } else{
                        this.player.addBonus(bonus.getBonus());
                        this.firstConquest = false;
                    }
                } else{
                    mapPanel.viewMessage("Hai già raggiunto il numero massimo di bonus!");
                    this.firstConquest = false;
                }
            }
        }

        //update the components in the map panel and defencing panel
        mapPanel.updateNArmiesOfTerritory(terAttack, terDefence);
        player.sendMessage("Press_next");
    }

      /* -----------------------------------------------------------------------------------------------------------

                                            method for SHIFT round

         -----------------------------------------------------------------------------------------------------------  */
    /**
     * Method to set the label of panel and the variable with the name and number armies of territory from which execute the shift
     *
     * @param idTerFrom    id territory from which execute the shift
     */
    public void setShift_TerritoryFROM(int idTerFrom){
        if(checkTerritoryOwner(idTerFrom, player)){
            playerPanel.getPnlAttackManager().getPnlShift().setLblFrom(mapPanel.getTerritories().get(idTerFrom).getName());
            playerPanel.getPnlAttackManager().getPnlShift().setLblNArmies(mapPanel.getTerritories().get(idTerFrom).getArmies());
            this.terShiftFrom = idTerFrom;
        }else{
            mapPanel.viewMessage("attenzione devi selezionare un tuo territorio!");
        }
    }

    /**
     * Method to set the label of panel and the variable with the name and number armies of territory to which execute the shift
     * after the check of owner player and of the adjacent
     *
     * @param idTerTo id territory to which execute the shift
     */
    public void setShift_TerritoryTO(int idTerTo){
        if(checkTerritoryOwner(idTerTo, player)){
            if(checkTerritoryAdjacent(idTerTo + 1, terShiftFrom)){
                playerPanel.getPnlAttackManager().getPnlShift().setLblTo(mapPanel.getTerritories().get(idTerTo).getName());
                playerPanel.getPnlAttackManager().getPnlShift().enabledBtnShiftPnl(true);
                this.terShiftTo = idTerTo;
            }else {
                mapPanel.viewMessage("attenzione devi selezionare un territorio adiacente a quello selezionato!");
            }
        }else{
            mapPanel.viewMessage("attenzione devi selezionare un tuo territorio!");
        }
    }

    /**
     * Method to get the id territory to which shift
     *
     * @return id of to territory shift
     */
    public int getShiftTo(){ return this.terShiftTo; }

    /**
     * Method to get the id territory from which shift
     *
     * @return  id of from territory shift
     */
    public int getShiftFrom(){ return this.terShiftFrom; }

    /**
     * Method to update the number of armies in the territories that the player selects for the shift
     * and update the component in the map panel
     *
     * @param nArmies   number of armies that player chose to shift
     */
    public void updateArmiesTerritory(int nArmies){

        mapPanel.addArmiesToTerritory(terShiftTo, nArmies);
        mapPanel.removeArmiesToTerritory(terShiftFrom, nArmies);
        mapPanel.updateNArmiesOfTerritory(terShiftFrom, terShiftTo);
    }



    /**
     * method to set the flags inherent the conquest of the territories
     *
     * @author Luca Genghini
     */
    public void setFlagConquest() {
        this.firstConquest = true;
        this.hasConquered = false;
    }

    /**
     * method to get if the player has conquered some territory during this turn
     *
     * @author Luca Genghini
     * @return true if the player has conquered at least a territory, false in the other case
     */
    public boolean getHasConquered(){
        return this.hasConquered;
    }

    /**
     * method to get the list of the bonus obtained from the player during the game
     *
     * @author Luca Genghini
     * @return the list of the bonus obtained from the player during the game
     */
    public List<Integer> getBonusList(){
        return this.player.getBonusList();
    }
}
