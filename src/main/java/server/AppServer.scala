package server

import java.awt.{Font, Toolkit}
import java.net.InetAddress
import java.util

import message._
import akka.actor._
import com.typesafe.config.ConfigFactory
import javax.swing.{JFrame, JLabel, JPanel, WindowConstants}
import risk.Player
import risk.target.Target

/** Factory for [[server.Server]] instances.
  *
  * @author Matteo Sertori
  *
  **/
object AppServer extends App {

  val panelServer = new JPanel
  val frameServer = new JFrame()
  val labelIpString = new JLabel

  val myIP = InetAddress.getLocalHost.getHostAddress
  val config = ConfigFactory.parseString(
    " akka { \n" +
      " actor { \n" +
      " enable-additional-serialization-bindings = on \n" +
      " akka.actor.allow-java-serialization = off \n" +
      " provider = remote\n" +
      "}\n" +
      " remote { \n" +
      " enabled-transports = [\"akka.remote.netty.tcp\"]\n" +
      " netty.tcp { \n" +
      " hostname = \"" + myIP + "\"\n" +
      " port = 5150\n" +
      "}\n" +
      "}\n" +
      "}\n")


  val system = ActorSystem("ServerSystem", config)

  val remoteActor = system.actorOf(Props[Server], name = "ActorServer")

  frameServer.setBounds(Toolkit.getDefaultToolkit.getScreenSize.width / 2, Toolkit.getDefaultToolkit.getScreenSize.height / 2, 300, 100)
  labelIpString.setText(myIP)

  labelIpString.setFont(new Font(labelIpString.getName, Font.PLAIN, 30))

  panelServer.add(labelIpString)
  frameServer.add(panelServer)
  frameServer.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)

  frameServer.setVisible(true)
}

/**
  * Class that manage all type of Messsage. Thought this class the game can communicate for two clients
  *
  * @author Matteo Sertori
  */
class Server extends Actor {

  private val listOnlinePlayer = new util.ArrayList[ActorRef]

  private var player1: Player = _
  private var player2: Player = _
  private var gameSetup: GameSetup = _
  private var territoriesList: util.ArrayList[util.ArrayList[_]] = _
  private var targetList : util.ArrayList[Target] = _
  private val messageArrived = new util.ArrayList[String]

  /**
    * Receive the message from the server or for the other player and manage the response
    */
  def receive: PartialFunction[Any, Unit] = {
     case msg:GenericMessage =>
      msg.getMessage match {
        case "LoadingMap" =>
          messageArrived.add("LoadingMap")

          if (messageArrived.size() == 2) {
            messageArrived.clear()

            listOnlinePlayer.get(0) ! new GenericMessage("StartGame")
          }
        case "GETNAMECLIENT" =>
          for (i <- 0 until listOnlinePlayer.size()) {
            if (listOnlinePlayer.get(i).equals(sender)) {

              sender ! new MsgNameClient(i)
            }
          }

          Thread.sleep(2000)

          if (listOnlinePlayer.size() == 2) {

            player1 = new Player
            player1.setOption(null, 0)

            player2 = new Player
            player2.setOption(null, 1)

            gameSetup = new GameSetup(player1, player2)

            territoriesList = gameSetup.createTerritory
            player1.populateTerritoriesList(territoriesList.get(0))
            player2.populateTerritoriesList(territoriesList.get(1))

            targetList = gameSetup.createTarget
            player1.setTarget(targetList.get(0))
            player2.setTarget(targetList.get(1))

            listOnlinePlayer.get(0) ! new MsgTargetCreated(targetList.get(0))
            listOnlinePlayer.get(1) ! new MsgTargetCreated(targetList.get(1))

            listOnlinePlayer.get(0) ! new TerritoriesPlayer(territoriesList.get(0), territoriesList.get(2))
            listOnlinePlayer.get(1) ! new TerritoriesPlayer(territoriesList.get(1), territoriesList.get(2))
          }
        case "CONNECTION" =>
          listOnlinePlayer.add(sender())
          sender ! new GenericMessage("CONNECTED")
        case _ =>
          sendMessage(msg)
      }
    case msg @ (_:MsgUpdateMap | _:MsgSendUpdateMap | _:MsgSendUpdateMapAfterAttack | _:MsgSendAttackCarried | _:MsgSendDiceResult | _:MsgSendShiftArmies | _:MsgDeleteTerritoryInList) => sendMessage(msg)

    case msg:MsgResultGame =>
      if (listOnlinePlayer.get(0).equals(sender)) {
        listOnlinePlayer.get(0) ! msg
        listOnlinePlayer.get(1) ! new MsgResultGame("LOSER")
      } else {
        listOnlinePlayer.get(1) ! msg
        listOnlinePlayer.get(0) ! new MsgResultGame("LOSER")

      }
  }

  /**
    * Method to send message received by client to the other client
    *
    * @param msg the message to send
    */
  def sendMessage(msg: Any): Unit = {
      if (listOnlinePlayer.get(0).equals(sender)) {
          listOnlinePlayer.get(1) ! msg
      } else {
          listOnlinePlayer.get(0) ! msg
      }
  }
}