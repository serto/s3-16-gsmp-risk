package server

import java.io._
import java.util
import java.util.StringTokenizer

import risk.target._
import risk.{Player, Territory}

import scala.util.Random

/**
  * Class used for setting the initial status of the game. This class is used for the initial creation and assignment of territories and
  * targets to the own player.
  *
  * @author Genghini Luca
  * @constructor create a new GameSetup taking in input the 2 players
  */

class GameSetup(private val player1: Player, private val player2: Player) {

  /**
    * method for the initial creation and assignment of territories. The details of each territory are read from a text file.
    *
    * @return a list which contains in the first position the list of territoris owned by player 1,
    *         in the second position the list of territories owned by player 2,
    *         and in the third position the global list with all territories
    */
  def createTerritory: util.ArrayList[util.ArrayList[_]] = {
      val territories = new util.ArrayList[Territory]
      val player1Territories = new util.ArrayList[Int]
      val player2Territories = new util.ArrayList[Int]
      val ret_val = new util.ArrayList[util.ArrayList[_]]

      try {
          //val file = new FileReader(new File("src/main/resources/Territori.txt").getAbsolutePath)

          val in = getClass.getResourceAsStream("/Territori.txt")
          val bufferedReader = new BufferedReader(new InputStreamReader(in))

          try {
              var territoryId = 0
              var countZero = 0
              var countOne = 0

              var line = ""
              while ({line = bufferedReader.readLine; line != null}) {

              if (line == null)
                throw new IOException

              val parser = new StringTokenizer(line, ":,()|")
              val territoryName = parser.nextToken
              val cordX = parser.nextToken.toInt
              val cordY = parser.nextToken.toInt
              val adjacents = new util.ArrayList[Integer]
              var nTokens = parser.countTokens

              while (nTokens != 0) {
                  adjacents.add(parser.nextToken.toInt)
                  nTokens -= 1
              }

              var playerOwner: Player = null
              val owner = new Random().nextInt(2)
              if (owner == 0)
                if (countZero < 21) {
                  countZero += 1
                  player1Territories.add(territoryId)
                  playerOwner = this.player1
               }
                else {
                  player2Territories.add(territoryId)
                  playerOwner = this.player2
                }
              else if (countOne < 21) {
                  countOne += 1
                  player2Territories.add(territoryId)
                  playerOwner = this.player2
              }
              else {
                  player1Territories.add(territoryId)
                  playerOwner = this.player1
              }

              val ter = new Territory(territoryName, adjacents, cordX, cordY, playerOwner)
              ter.addArmies(1)
              territories.add(ter)

              territoryId += 1

          }
        } catch {
            case _:IOException =>
              try
                  bufferedReader.close()
              catch {
                  case e: IOException =>
                    e.printStackTrace()
              }

        }
      } catch {
          case e: FileNotFoundException =>
              e.printStackTrace()
      }

      ret_val.add(player1Territories)
      ret_val.add(player2Territories)
      ret_val.add(territories)

      ret_val
  }

  /**
    * method for the creation and assignment of targets to the player.
    *
    * @return a list which contains the two targets of the player, one for each player.
    */
  def createTarget: util.ArrayList[Target] = {
    val ret_val = new util.ArrayList[Target]
    var i = 0
    while (i<=2){
      val targetId = new Random().nextInt(4)
      targetId match {
        case 0 =>
          ret_val.add(new Target30Territories)
        case 1 =>
          ret_val.add(new TargetNA_A)
        case 2 =>
          ret_val.add(new TargetSA_E)
        case 3 =>
          ret_val.add(new TargetThreeContinents)
      }
      i += 1
    }
    ret_val
  }
}